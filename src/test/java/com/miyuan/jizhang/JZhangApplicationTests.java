package com.miyuan.jizhang;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSONObject;
import com.miyuan.jizhang.request.InsertGroupRequestVo;
import com.miyuan.jizhang.service.GroupService;
import com.miyuan.jizhang.utils.Base64Utils;
import com.miyuan.jizhang.utils.HttpClientUtils;
import com.miyuan.jizhang.utils.Md5Utils;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JZhangApplicationTests {

	@Autowired
	private GroupService groupService;

	@Test
	public void contextLoads() {
		InsertGroupRequestVo vo = new InsertGroupRequestVo();
		vo.setName("eqwkldadia");
		vo.setDesc("klxxkklcxklckl");
		try {
			groupService.addGroup111(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
//		String data = new String("admin123");
//		String encode = Base64Utils.encode(data);
//		System.out.println(encode);
//		String encryptPassword = Md5Utils.encryptPassword(encode, Md5Utils.salt);
//		System.out.println(encryptPassword);// YWRtaW4xMjM=
//		
//		
//		JSONObject jsonParam = JSONObject.parseObject("{\"username\":\"kedacom\",\"password\":\"kedacom\"}");
//		JSONObject doPost = HttpClientUtils.httpPost("http://172.16.128.224/iamsweb/Login", jsonParam );
//		System.out.println(doPost);
//		
//		System.out.println(new String(Md5Utils.encryptPassword("admin123", Md5Utils.salt))); 
		
		System.out.println(secToTime("890.0"));
	}

	public static String secToTime(String time) {
		if (StringUtils.isBlank(time)) {
			return "";
		}
		if (time.contains(".")) {
			time = time.substring(0, time.indexOf("."));
		}
		long t = Long.parseLong(time);
		long ms = t * 1000;// 毫秒数
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");// 初始化Formatter的转换格式。
		return formatter.format(ms);
	}
}
