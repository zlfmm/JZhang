<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />
<title>米缘.记一笔</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<!-- https://fonts.google.com/specimen/Open+Sans -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fontawesome.min.css">
<!-- https://fontawesome.com/ -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fullcalendar.min.css">
<!-- https://fullcalendar.io/ -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css">
<!-- https://getbootstrap.com/ -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/tooplate.css">
</head>
<body>
	<div class="" id="home">
		<div class="container">
			<jsp:include page="/jsp/head.jsp"></jsp:include>
			<!-- row -->
			<div class="row tm-content-row tm-mt-big">
				<div class="tm-col tm-col-big">
					<div class="bg-white tm-block h-100">
						<h2 class="tm-block-title">支出统计折线图</h2>
						<canvas id="lineChart"></canvas>
					</div>
				</div>
				<div class="tm-col tm-col-big">
					<div class="bg-white tm-block h-100">
						<h2 class="tm-block-title">支出统计柱状图</h2>
						<canvas id="barChart"></canvas>
					</div>
				</div>
				<div class="tm-col tm-col-small">
					<div class="bg-white tm-block h-100">
						<h2 class="tm-block-title">支出统计饼状图</h2>
						<canvas id="pieChart" class="chartjs-render-monitor"></canvas>
					</div>
				</div>
			</div>
		</div>

		<script src="<%=request.getContextPath()%>/bootstrap/js/jquery-3.3.1.min.js"></script>
		<!-- https://jquery.com/download/ -->
		<script src="<%=request.getContextPath()%>/bootstrap/js/moment.min.js"></script>
		<!-- https://momentjs.com/ -->
		<script src="<%=request.getContextPath()%>/bootstrap/js/utils.js"></script>
		<script src="<%=request.getContextPath()%>/bootstrap/js/Chart.min.js"></script>
		<!-- http://www.chartjs.org/docs/latest/ -->
		<script src="<%=request.getContextPath()%>/bootstrap/js/fullcalendar.min.js"></script>
		<!-- https://fullcalendar.io/ -->
		<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
		<!-- https://getbootstrap.com/ -->
		<script src="<%=request.getContextPath()%>/bootstrap/js/tooplate-scripts.js"></script>
		<script>
			// 初始化变量
			let ctxLine, ctxBar, ctxPie, optionsLine, optionsBar, optionsPie, configLine, configBar, configPie, lineChart;
			barChart, pieChart;
			
			var lineChartLabels = [];
			var lineChartDatasets = [];
			var barChartLabels = [];
			var barChartDatasets = [];
			var pieChartLabels = [];
			var pieChartDatasets = [];
			
			var colors = [ "rgba(255, 99, 132, 0.2)",
				"rgba(54, 162, 235, 0.2)",
				"rgba(255, 206, 86, 0.2)",
				"rgba(75, 192, 192, 0.2)",
				"rgba(153, 102, 255, 0.2)",
				"rgba(255, 159, 64, 0.2)",
				"rgba(255, 159, 111, 0.2)",
				"rgba(255, 159, 24, 0.2)"];
			
			$(function() {
				// 折线图横坐标数据
				/* var lineChartLabels = [ "January1", "February1", "March1", "April", "May", "June", "July" ];
				var lineChartDatasets = [ {
					label : "Latest Hits",
					data : [ 88, 68, 79, 57, 56, 55, 70 ],
					fill : false,
					borderColor : "rgb(75, 192, 192)",
					lineTension : 0.1
				}, {
					label : "Popular Hits",
					data : [ 33, 45, 37, 21, 55, 74, 69 ],
					fill : false,
					borderColor : "rgba(255,99,132,1)",
					lineTension : 0.1
				}, {
					label : "Featured",
					data : [ 44, 19, 38, 46, 85, 66, 79 ],
					fill : false,
					borderColor : "rgba(153, 102, 255, 1)",
					lineTension : 0.1
				} ]; */

				// 柱状图横坐标数据
				/* var barChartLabels = [ "Red", "Blue", "Yellow", "Green",
						"Purple", "Orange" ];
				var barChartDatasets = [ {
					label : "# of Hits",
					data : [ 12, 19, 3, 5, 2, 3 ],
					backgroundColor : [ "rgba(255, 99, 132, 0.2)",
							"rgba(54, 162, 235, 0.2)",
							"rgba(255, 206, 86, 0.2)",
							"rgba(75, 192, 192, 0.2)",
							"rgba(153, 102, 255, 0.2)",
							"rgba(255, 159, 64, 0.2)" ],
					borderColor : [ "rgba(255,99,132,1)",
							"rgba(54, 162, 235, 1)", "rgba(255, 206, 86, 1)",
							"rgba(75, 192, 192, 1)", "rgba(153, 102, 255, 1)",
							"rgba(255, 159, 64, 1)" ],
					borderWidth : 2
				} ]; */

				// 饼状图横坐标数据
				/* var pieChartLabels = [ "Used: 4,600 GB", "Available: 5,400 GB" ];
				var pieChartDatasets = [ {
					data : [ 4600, 5400 ],
					backgroundColor : [ window.chartColors.purple,
							window.chartColors.green ],
					label : "Storage"
				} ]; */
				
				// 初始化折线图数据
				initLineChart();
				
				// 初始化柱状图数据
				initBarChart();
				
				// 初始化饼状图数据
				initPirChart();

				updateChartOptions();

				$(window).resize(function() {
					updateChartOptions();
					updateLineChart();
					updateBarChart();
					reloadPage();
				});
			})
			
			// 初始化折线图数据
			function initLineChart() {
				$.ajax({
					contentType: "application/json;charset-UTF-8",
	  			  	url : '<%=request.getContextPath()%>/money/countMoneyLineChart',
	  				type : 'POST',
	  				async: true,
	  				dataType : "json",
	  				success : function(data) {
	  					var list = data.data;
	  					var datasets = [];
	  					if (data.errCode == 200) {
	  						for ( var i in list) {
	  							lineChartLabels.push(list[i].label);
	  							datasets.push(list[i].dataset);
							}
	  						lineChartDatasets = [ {
		  						label : "支出（金额）",
		  						data : datasets,
		  						fill : true,
		  						borderColor : "rgb(75, 192, 192)",
		  						lineTension : 0.1
		  					}];
						} else {
							datasets.push(0);
							lineChartDatasets = [ {
		  						label : "支出（金额）",
		  						data : datasets,
		  						fill : true,
		  						borderColor : "rgb(75, 192, 192)",
		  						lineTension : 0.2
		  					}];
						}
	  					drawLineChart(lineChartLabels, lineChartDatasets); // Line Chart
	  				},
	  				error : function(data) {
	  					alert(data);
	  				}
				});
			}
			
			// 初始化柱状图数据
			function initBarChart() {
				$.ajax({
					contentType: "application/json;charset-UTF-8",
	  			  	url : '<%=request.getContextPath()%>/money/countMoneyBarChart',
	  				type : 'POST',
	  				async: true,
	  				dataType : "json",
	  				success : function(data) {
	  					var list = data.data;
	  					var datasets = [];
	  					var backgroundColor = [];
	  					var borderColor = [];
	  					if (data.errCode == 200) {
	  						for ( var i in list) {
	  							barChartLabels.push(list[i].label);
	  							datasets.push(list[i].dataset);
	  							if (i <= 1) {
	  								backgroundColor.push(colors[i]);
	  								borderColor.push(colors[i]);
								} else {
									var index = i % 8;
									backgroundColor.push(colors[index]);
									borderColor.push(colors[index]);
								}
							}
						} else {
							datasets.push(0);
						}
	  					barChartDatasets = [ {
	  						label : "支出（金额）",
	  						data : datasets,
	  						backgroundColor: backgroundColor,
	  						borderColor: borderColor,
	  						borderWidth : 2
	  					} ];
	  					drawBarChart(barChartLabels, barChartDatasets); // Bar Chart
	  				},
	  				error : function(data) {
	  					alert(data);
	  				}
				});
			}
			
			// 初始化饼状图数据
			function initPirChart() {
				$.ajax({
					contentType: "application/json;charset-UTF-8",
	  			  	url : '<%=request.getContextPath()%>/money/countMoneyPieChart',
	  				type : 'POST',
	  				async: true,
	  				dataType : "json",
	  				success : function(data) {
	  					var list = data.data;
	  					var datasets = [];
	  					var chartColors = [];
	  					if (data.errCode == 200) {
	  						for ( var i in list) {
	  							pieChartLabels.push(list[i].label);
	  							datasets.push(list[i].dataset);
	  							if (i <= 1) {
	  								chartColors.push(colors[i]);
								} else {
									var index = i % 8;
									chartColors.push(colors[index]);
								}
							}
						} else {
							datasets.push(0);
						}
	  					pieChartDatasets = [ {
	  						data : datasets,
	  						backgroundColor : chartColors,
	  						label : "支出（金额）"
	  					} ];
	  					drawPieChart(pieChartLabels, pieChartDatasets); // Pie Chart
	  				},
	  				error : function(data) {
	  					alert(data);
	  				}
				});
			}
		</script>
</body>
</html>