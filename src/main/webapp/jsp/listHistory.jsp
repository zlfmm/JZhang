<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>

<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>米缘.记一笔</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fontawesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/pagination.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap-selection.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/daterangepicker-bs3.css">
</head>
<body id="reportsPage" class="bg02">
	<div class="" id="home">
		<div class="container">
			<jsp:include page="/jsp/head.jsp"></jsp:include>
			<!-- row -->
			<div class="row tm-content-row tm-mt-big">
				<div class="col-xl-12 col-lg-12 tm-md-12 tm-sm-12 tm-col">
					<div class="bg-white tm-block h-100">
						<div class="row">
							<div class="col-md-4 col-sm-12">
								<h2 class="tm-block-title d-inline-block">往期账单</h2>
							</div>
							<div class="col-md-3 col-sm-12 text-right">
								<div class="form-group">
									<form class="form-horizontal">
										<div class="input-prepend input-group">
											<input type="text" name="reservation"
												id="reservationtime" class="form-control"
												value="请选择时间查询" class="span4" />
										</div>
									</form>
								</div>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-right">
								<button type="button" class="btn btn-primary btn-sm add_btn">查询</button>
							</div> -->
						</div>
						<div class="table-responsive">
							<table
								class="table table-hover table-striped tm-table-striped-even mt-3">
								<thead>
									<tr class="tm-bg-gray">
										<th scope="col">消费人</th>
										<th scope="col" class="text-center">消费金额</th>
										<th scope="col" class="text-center">消费时间</th>
										<th scope="col" class="text-center">结算时间</th>
										<th scope="col" class="text-center">登记时间</th>
										<th scope="col">备注</th>
									</tr>
								</thead>
								<tbody class="table-tbody-class"></tbody>
							</table>
						</div>

						<div class="tm-table-mt tm-table-actions-row">
							<div class="tm-table-actions-col-right">
								<div>
									<div id="Pagination" class="pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.selection.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.pagination.js" charset="UTF-8"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/underscore-min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/moment.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/daterangepicker.js"></script>

	<script type="text/javascript">
	    var pageSize = 10;//每页显示多少条记录
	    var total;
	    $(function () {
        	// 初始化列表
        	calllist(1, null, null);
        	
        	$('#reservationtime').daterangepicker({
        		timePicker: true,
        		timePickerIncrement: 30,
        		format: 'MM/DD/YYYY h:mm A'
            }, function(start, end, label) {
            	var startTime = formatDate(start.toISOString(), 'yyyy-MM-dd hh:mm:ss');
            	var endTime = formatDate(end.toISOString(), 'yyyy-MM-dd hh:mm:ss');
            	calllist(1, startTime, endTime);
            	
            	// 初始化分页插件
            	$("#Pagination").pagination({
        			pageCount: total,
        			callback: function(index){
        				calllist(index.getCurrent());
        			},
        			jump: true, // 跳转到指定页数
        			jumpBtn: '跳转', // 跳转按钮文本
        			coping: false, // 首页和尾页
        			isHide: true, // 当前页数为0页或者1页时不显示分页
        			prevContent: ' 上一页 ',
        	        nextContent: ' 下一页 ',
        			items_per_page: pageSize,
        			num_display_entries: 0, // 连续分页主体部分显示的分页条目数
        			num_edge_entries: 0, // 两侧显示的首尾分页的条目数
        		});
            	
            });
        	
        	// 初始化分页插件
        	$("#Pagination").pagination({
    			pageCount: total,
    			callback: function(index){
    				calllist(index.getCurrent());
    			},
    			jump: true, // 跳转到指定页数
    			jumpBtn: '跳转', // 跳转按钮文本
    			coping: false, // 首页和尾页
    			isHide: true, // 当前页数为0页或者1页时不显示分页
    			prevContent: ' 上一页 ',
    	        nextContent: ' 下一页 ',
    			items_per_page: pageSize,
    			num_display_entries: 0, // 连续分页主体部分显示的分页条目数
    			num_edge_entries: 0, // 两侧显示的首尾分页的条目数
    		});
        	
        })
        
        /*查询列表*/
        function calllist(pageIndex, startTime, endTime){
        	var object = {
     			"pageNumber" : pageIndex,
     			"pageSize" : pageSize,
     			"isSettle" : 1,
     			"payStartTime": startTime,
     			"payEndTime": endTime
   			};
            $.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/money/list',
  				type : 'POST',
  				data : JSON.stringify(object),
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data.data;
  						total = Math.ceil(data.data.total / pageSize); // 当前栏目下的总数
  						var tbody='';
  	  					for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td hidden>' + list[i].id + '</td>';
  	  	  					tbody += '<td>' + list[i].realName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].money + '元</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].payTime + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].settleTime + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].createTime + '</td>';
  	  	  					tbody += '<td style="max-width: 350px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' + list[i].desc + '</td>';
  	  	  					tbody += '</tr>';
  						}
  		  				//加载数据到页面的 <tbody>标签中
  		  				$('.table-tbody-class')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
        }
	    
	    /*格式化日期*/
	    function formatDate(date, fmt) {
	    	var currentDate = new Date(date);
	    	var o = {
	    			"M+": currentDate.getMonth() + 1, //月份
		    	    "d+": currentDate.getDate(), //日
		    	    "h+": currentDate.getHours(), //小时
		    	    "m+": currentDate.getMinutes(), //分
		    	    "s+": currentDate.getSeconds(), //秒
		    	    "q+": Math.floor((currentDate.getMonth() + 3) / 3), // 季度
		    	    "S": currentDate.getMilliseconds() //毫秒
	    	    };
	    	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (currentDate.getFullYear() + "").substr(4 - RegExp.$1.length));
	    	for (var k in o)
	    		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    	return fmt;
    	}
    </script>
</body>
</html>