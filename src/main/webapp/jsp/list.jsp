<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>米缘.记一笔</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fontawesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/pagination.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap-select.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap-datetimepicker.css">
</head>
<body id="reportsPage" class="bg02">
	<div class="" id="home">
		<div class="container">
			<jsp:include page="/jsp/head.jsp"></jsp:include>

			<!-- row -->
			<div class="row tm-content-row tm-mt-big">
				<div class="col-xl-8 col-lg-12 tm-md-12 tm-sm-12 tm-col">
					<div class="bg-white tm-block h-100">
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<h2 class="tm-block-title d-inline-block">本期账单</h2>
							</div>
							<div class="col-md-4 col-sm-12 text-right">
								<button type="button" class="btn btn-info btn-sm add_btn">记一笔</button>
							</div>
						</div>
						<div class="table-responsive">
							<table
								class="table table-hover table-striped tm-table-striped-even mt-3">
								<thead>
									<tr class="tm-bg-gray">
										<th scope="col" hidden>ID</th>
										<th scope="col"><input type="checkbox" class="checkall"
											aria-label="Checkbox"></th>
										<th scope="col">消费人</th>
										<th scope="col" class="text-center">消费金额</th>
										<th scope="col" class="text-center">消费时间</th>
										<th scope="col">备注</th>
										<th scope="col">操作</th>
									</tr>
								</thead>
								<tbody class="table-tbody-class"></tbody>
							</table>
						</div>

						<div class="tm-table-mt tm-table-actions-row">
							<div class="tm-table-actions-col-left">
								<button class="btn btn-danger more_delete">删除选中账单</button>
							</div>
							<div class="tm-table-actions-col-right">
								<div>
									<div id="Pagination" class="pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-12 tm-md-12 tm-sm-12 tm-col">
					<div class="bg-white tm-block h-100">
						<div class="row">
							<div class="col-md-3 col-sm-12">
								<h2 class="tm-block-title d-inline-block">群组名:</h2>
							</div>
							<div class="col-md-9 col-sm-12 text-right">
								<button type="button" class="btn btn-default btn-sm add_group_member">请君入瓮</button>
								<button type="button" class="btn btn-default btn-sm add_group">编辑</button>
								<button type="button" class="btn btn-warn btn-sm delete_group">解散</button>
							</div>
						</div>
						<h2 class="tm-block-title d-inline-block group_name_class"></h2>
						<table class="table table-hover table-striped mt-3">
							<tbody class="member_list_class"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- 记一笔-模态框（Modal） -->
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true" style="top: 60px;">
		<div class="modal-dialog" style="max-width: 36%;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">记一笔</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="group" class="col-sm-5 control-label">请选择组群</label>
						<div class="col-sm-10">
							<select id="group" class="qz_selectpicker form-control"
								style="padding: 12px 18px;" data-style="btn-danger"></select>
						</div>
					</div>
					<div class="form-group">
						<label for="money" class="col-sm-5 control-label">请输入金额</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="money"
								placeholder="如：10.2"
								onkeyup="this.value=this.value.replace(/[^\d.]/g,'')"
								onafterpaste="this.value=this.value.replace(/[^\d.]/g,'')">
						</div>
					</div>
					<div class="form-group">
						<label for="desc" class="col-sm-5 control-label">请输入备注</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="desc"
								placeholder="如：买的饺子">
						</div>
					</div>
					<div class="form-group">
						<label for="payTime" class="col-sm-5 control-label">请选择支付时间</label>
						<div class="col-sm-10">
							<div class="input-append date datetimepicker">
								<input id="payTime" class="form-control" type="text"
									placeholder="YYYY-MM-DD hh:mm:ss" readonly> <span
									class="add-on"><i class="icon-remove"></i></span> <span
									class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm"
						data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary btn-sm"
						id="addModal_btn">提交</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>



	<!-- 编辑分组-模态框（Modal） -->
	<div class="modal fade" id="addGroupModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalGroupLabel" aria-hidden="true"
		style="top: 60px;">
		<div class="modal-dialog" style="max-width: 36%;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">编辑群组</h4>
				</div>
				<div class="modal-body">
					<input type="text" id="group_id" style="display: none;" value="" />
					<div class="form-group">
						<label for="group_name" class="col-sm-5 control-label">请输入群组名称</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="group_name"
								placeholder="如：会飞的猪">
						</div>
					</div>
					<div class="form-group">
						<label for="group_desc" class="col-sm-5 control-label">请输入备注</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="group_desc"
								placeholder="如：会飞的猪的备注">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm"
						data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary btn-sm"
						id="addGroupModal_btn">提交</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>


	<!-- 请君入瓮-模态框（Modal） -->
	<div class="modal fade" id="addMemberModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalMemberLabel" aria-hidden="true"
		style="top: 60px;">
		<div class="modal-dialog" style="max-width: 36%;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">请君入瓮</h4>
				</div>
				<div class="modal-body">
					<input type="text" id="address_id" style="display: none;" value="" />
					<div class="form-group">
						<label for="address_select" class="col-sm-5 control-label">搜索被邀请人邮箱</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="address_input"
								placeholder="如：1931787289@qq.com">
							<select id="address_select" class="form-control" style="padding: 12px 18px;"></select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm"
						data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary btn-sm"
						id="addMenberModal_btn">提交</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.selection.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.pagination.js" charset="UTF-8"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/underscore-min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap-datetimepicker.zh-CN.js"
		charset="UTF-8"></script>

	<script type="text/javascript">
	    var pageSize = 10;//每页显示多少条记录
	    var total;
	    
        $(function () {
        	
        	// 初始化列表
        	calllist(1);
        	
        	// 初始化群成员信息
        	callMembers();
        	
        	// 初始化分页插件
        	$("#Pagination").pagination({
    			pageCount: total,
    			callback: function(index){
    				if ($('.checkall').is(':checked')) {
    					$(".checkall").prop("checked", false);
    					$(".checkb").prop("checked", false);
    				}
    				calllist(index.getCurrent());
    			},
    			jump: true, // 跳转到指定页数
    			jumpBtn: '跳转', // 跳转按钮文本
    			coping: false, // 首页和尾页
    			isHide: true, // 当前页数为0页或者1页时不显示分页
    			prevContent: ' 上一页 ',
    	        nextContent: ' 下一页 ',
    			items_per_page: pageSize,
    			num_display_entries: 0, // 连续分页主体部分显示的分页条目数
    			num_edge_entries: 0, // 两侧显示的首尾分页的条目数
    		});
        	
        });
        
        $('.datetimepicker').datetimepicker({
            format:'yyyy-mm-dd hh:ii:ss',
            language:'zh-CN',
            startView:'year',
            maxView:'year',
            minView:'hour',
            autoclose: true,
            pickerPosition: "bottom-left",
            todayBtn:true,
            minuteStep:1
        });

        /*查询列表*/
        function calllist(pageIndex){
        	var object = {
     			"pageNumber" : pageIndex,
     			"pageSize" : pageSize,
     			"isSettle" : 0,
   			};
            $.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/money/list',
  				type : 'POST',
  				data : JSON.stringify(object),
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data.data;
  						total = Math.ceil(data.data.total / pageSize); // 当前栏目下的总数
  						var tbody='';
  	  					for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td hidden>' + list[i].id + '</td>';
  	  	  					tbody += '<th scope="row"><input type="checkbox" class="checkb" value="' + list[i].id + '" aria-label="Checkbox"></th>';
  	  	  					tbody += '<td>' + list[i].realName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].money + '元</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].payTime + '</td>';
  	  	  					tbody += '<td style="max-width: 350px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' + list[i].desc + '</td>';
  	  	  					tbody += '<td><i class="fas fa-trash-alt tm-trash-icon money-delete"></i></td>';
  	  	  					tbody += '</tr>';
  						}
  		  				//加载数据到页面的 <tbody>标签中
  		  				$('.table-tbody-class')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
        }
        
        function callMembers() {
        	$.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/group/findMembers',
  				type : 'POST',
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data;
  						var tbody='';
						for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td>' + list[i].realName + '</td>';
  	  	  					tbody += '<td>' + list[i].createTime + '</td>';
  	  	  					tbody += '</tr>';
  						}
  	  					var group_value = list[0].groupName;
  	  					$('.group_name_class').text(group_value);
  	  					//加载数据到页面的 <tbody>标签中
  		  				$('.member_list_class')[0].innerHTML = tbody;
					} else {
						alert(data.errMsg);
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
		}

        /*初始化下拉群组数据*/
        function initSelectOptions(){
			var object = {};
			$.ajax({
				contentType: "application/json;charset-UTF-8",
				url : '<%=request.getContextPath()%>/group/list',
				type : 'POST',
				data : JSON.stringify(object),
				async: false,
				dataType : "json",
				success : function(data) {
					if (data.errCode == 200) {
						var list = data.data.data;
						var tbody = '';
                        for ( var i in list) {
                        	tbody += '<option value="'+list[i].id+'">' + list[i].aname + '</option>';
						}
                        $('.qz_selectpicker')[0].innerHTML = tbody;
					} else {
						alert(data.errMsg);
					}
				},
				error : function(data) {
					alert(data);
				}
			});
		}

        /*点击记一笔按钮*/
        $('.add_btn').click(function(){
        	// 初始化下拉框
			initSelectOptions();
        	$('#addModal').modal('show');
        });

        
        /*单条删除*/
        $('.table-tbody-class').on('click', '.money-delete', function(){
            var id = $(this).parent().parent().find('td').eq(0).text();
            var datas = [];
            datas.push(id);
            if(confirm("是否确定删除该条数据?")){
            	deletefunc(datas);
            }
        });
        
        /*多条删除*/
        $('.more_delete').click(function() {
        	var datas = [];
        	var count = 0;
        	$(".checkb").each(function(){
			    if($(this).is(':checked')){
			    	datas.push($(this).val());
			    	count++;
			    }
		    });
        	if (typeof datas !=='undefined' && datas.length>0) {
        		if(confirm("确定删除这" + count + "条记录吗?")){
        			deletefunc(datas);
    			}
			} else {
				alert('请先选中要删除的项');
			}
		});
        
        function deletefunc(datas) {
        	$.ajax({
        		contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/money/delete',
  				type : 'POST',
  				data : JSON.stringify({
                    'ids': datas
  				}),
  				async : true,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						location.reload();
					} else {
						alert(data.errMsg);
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
		}
        
        /*全选*/
        $('.checkall').click(function(){
        	if ($('.checkall').is(':checked')) {
        		$(".checkb").prop("checked", true);
			} else {
				$(".checkb").prop("checked", false);
			}
        });
        
        /*添加消费记录*/
        $('#addModal_btn').click(function(){
        	var money = $('#money').val();
        	var desc = $('#desc').val();
        	var payTime = $('#payTime').val();
            var userGroupId = $('.qz_selectpicker option:selected').val();
            $.ajax({
                contentType: "application/json;charset-UTF-8",
                url : '<%=request.getContextPath()%>/money/add',
                type : 'POST',
                data : JSON.stringify({
                    'userGroupId': userGroupId,
                    'money': money,
                    'payTime' : payTime,
                    'desc' : desc
                }),
                async : true,
                dataType : "json",
                success : function(data) {
                	if (data.errCode == 200) {
                        var money = $('#money').val("");
                    	var desc = $('#desc').val("");
                    	var payTime = $('#payTime').val("");
                        $('#addModal').modal('hide');
                        location.reload();
					} else {
						alert(data.errMsg + ":" + JSON.stringify(data.data));
					}
                },
                error : function(data) {
                    alert(data);
                }
            });
        });
        
        $('.add_group').click(function(){
        	
        	initGroupMsg();
        	
        	$('#addGroupModal').modal('show');
        });
        
        /* 初始化分组信息*/
        function initGroupMsg(){
        	// 查询群组信息
        	$.ajax({
                contentType: "application/json;charset-UTF-8",
                url : '<%=request.getContextPath()%>/group/list',
                type : 'POST',
                data : JSON.stringify({}),
                async : true,
                dataType : "json",
                success : function(data) {
                	var list = data.data.data;
                	if (data.errCode == 200) {
                		if (null != list) {
                			$('#group_id').val(list[0].id);
                			$('#group_name').val(list[0].aname);
                			$('#group_desc').val(list[0].adesc);
						}
					}
                },
                error : function(data) {
                    alert(data);
                }
            });
        }
        
        /* 编辑群组*/
        $('#addGroupModal_btn').click(function(){
        	var name = $('#group_name').val();
        	var desc = $('#group_desc').val();
        	var url = '';
        	var data = '';
        	var id = $('#group_id').val();
        	if (null == id || '' == id) {
        		url = '<%=request.getContextPath()%>/group/add';
        		data = JSON.stringify({
                    'name': name,
                    'desc' : desc
                });
			} else {
				url = '<%=request.getContextPath()%>/group/updatebyid';
				data = JSON.stringify({
                    'name': name,
                    'desc' : desc,
                    'id' : id
                });
			}
        	$.ajax({
                contentType: "application/json;charset-UTF-8",
                url : url,
                type : 'POST',
                data : data,
                async : false,
                dataType : "json",
                success : function(data) {
                	if (data.errCode == 200) {
                        location.reload();
					} else {
						alert(data.errMsg + ":" + JSON.stringify(data.data));
					}
                },
                error : function(data) {
                    alert(data);
                }
            });
		});
        
        /* 请君入瓮按钮*/
        $('.add_group_member').click(function(){
        	$('#addMemberModal').modal('show');
        });
        
        /* 请君入瓮 邮箱搜索*/
        $('#address_input').bind('input propertychange',function () {
        	var value = $(this).val();
        	$.ajax({
                contentType: "application/x-www-form-urlencoded",
                url : '<%=request.getContextPath()%>/userinfo/listUserInfoByEmail',
                type : 'POST',
                data : {
                	address: value
        		},
                async : true,
                dataType : "json",
                success : function(data) {
                	var list = data.data;
                	if (data.errCode == 200) {
                        var tbody = '';
                        for ( var i in list) {
                        	tbody += '<option value="'+list[i].id+'">' + list[i].userAddress + '</option>';
						}
                        $('#address_select')[0].innerHTML = tbody;
					} else {
						alert(data.errMsg);
					}
                },
                error : function(data) {
                    alert(data);
                }
            });
        });
        
        /* 请君入瓮确定按钮*/
        $('#addMenberModal_btn').click(function() {
        	var uId = $('#address_select option:selected').val();
        	$.ajax({
                contentType: "application/x-www-form-urlencoded",
                url : '<%=request.getContextPath()%>/group/invitateUser',
                type : 'POST',
                data : {
                	uId: uId
        		},
                async : false,
                dataType : "json",
                success : function(data) {
                	var list = data.data;
                	if (data.errCode == 200) {
                		$('#addMemberModal').modal('hide');
                        location.reload();
					} else {
						alert(data.errMsg);
					}
                },
                error : function(data) {
                    alert(data);
                }
            });
		});
        
        /* 解散群组*/
        $('.delete_group').click(function(){
        	if(confirm("是否确定解散该群组?")){
        		$.ajax({
                    contentType: "application/json;charset-UTF-8",
                    url : '<%=request.getContextPath()%>/group/dissoluteGroup',
                    type : 'POST',
                    async : false,
                    dataType : "json",
                    success : function(data) {
                    	if (data.errCode == 200) {
                            location.reload();
    					} else {
    						alert(data.errMsg);
    					}
                    },
                    error : function(data) {
                        alert(data);
                    }
                });
            }
        });
    </script>

</body>
</html>