<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<html>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<!-- https://fonts.google.com/specimen/Open+Sans -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fontawesome.min.css">
<!-- https://fontawesome.com/ -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fullcalendar.min.css">
<!-- https://fullcalendar.io/ -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css">
<!-- https://getbootstrap.com/ -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/tooplate.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
	<div class="row">
		<div class="col-12">
			<nav class="navbar navbar-expand-xl navbar-light bg-light"> <a
				class="navbar-brand" href="#"> <!-- <i class="fas fa-3x fa-tachometer-alt tm-site-icon"></i> -->
				<h1 class="tm-site-title mb-0">米缘.记一笔</h1>
			</a>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mx-auto tab_ul">
					<li class="nav-item"><a class="nav-link main"
						href="<%=basePath%>toindex">首页 <span class="sr-only">(current)</span>
					</a></li>
					<!-- <li class="nav-item dropdown">
	                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
	                            aria-expanded="false">
	                            Reports
	                        </a>
	                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	                            <a class="dropdown-item" href="#">Daily Report</a>
	                            <a class="dropdown-item" href="#">Weekly Report</a>
	                            <a class="dropdown-item" href="#">Yearly Report</a>
	                        </div>
	                    </li> -->
					<li class="nav-item"><a class="nav-link list"
						href="<%=basePath%>tolist">记一笔</a></li>
					<li class="nav-item"><a class="nav-link list_history"
						href="<%=basePath%>tohistory">支出查账</a></li>
					<li class="nav-item"><a class="nav-link settlement"
						href="<%=basePath%>tosettlement">结算</a></li>
				</ul>
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link d-flex"> <i
							class="far fa-user mr-2 tm-logout-icon"></i> <span
							onclick="logout();">${curUser.realName},Logout</span>
					</a></li>
				</ul>
			</div>
			</nav>
		</div>
	</div>

	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery-3.3.1.min.js"></script>
	<!-- https://jquery.com/download/ -->
	<script src="<%=request.getContextPath()%>/bootstrap/js/moment.min.js"></script>
	<!-- https://momentjs.com/ -->
	<script src="<%=request.getContextPath()%>/bootstrap/js/utils.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/Chart.min.js"></script>
	<!-- http://www.chartjs.org/docs/latest/ -->
	<script src="<%=request.getContextPath()%>/bootstrap/js/fullcalendar.min.js"></script>
	<!-- https://fullcalendar.io/ -->
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<!-- https://getbootstrap.com/ -->
	<script src="<%=request.getContextPath()%>/bootstrap/js/tooplate-scripts.js"></script>

	<script type="text/javascript">
    
	    $(function(){
	    	saveTabActive();
	    });
	    
	    function saveTabActive() {
	    	var strIndex = null;
	    	$('ul.tab_ul li').click(function(){
				window.localStorage.setItem('index',$(this).index());
	    	});
	    	strIndex = window.localStorage.getItem('index');
	    	if(strIndex == null){
	    		$("ul.tab_ul li:first").addClass("active");
	    	} else {
	    		$("ul.tab_ul li").eq(strIndex).addClass("active");
	    		$("ul.tab_ul li").eq(strIndex).siblings().removeClass("active");
	    		window.localStorage.removeItem('index');
	    	}
		}
	    
	    function logout() {
	        $.ajax({
	            url:"<%=basePath%>logout",
	            type : 'GET',
	            success:function(data){
	            	if (data.errCode == 200) {
						window.location.href = "<%=basePath%>tologin";
					} else {
						alert(data.errMsg);
					}
				},
				error : function(data) {
					alert(data.errMsg);
				}
			});
		}
	</script>
</body>
</html>