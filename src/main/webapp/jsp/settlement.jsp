<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>

<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>米缘.记一笔</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fontawesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/pagination.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap-selection.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="/bootstrap/css/bootstrap-datetimepicker.css">
<link href="/resources/assets1/css/style.css" rel="stylesheet" />
</head>
<body>
<body id="reportsPage" class="bg02">
	<div class="" id="home">
		<div class="container">
			<jsp:include page="/jsp/head.jsp"></jsp:include>
			<div class="row" style="margin-top: 5%;">
				<div class="col-md-5 col-sm-5" style="margin-left: 8%;">
					<div class="panel panel-info">
						<div class="panel-heading"><h1>总消费</h1>(本期)</div>
						<div class="panel-body">
							<h1 class="total_h1"></h1>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-sm-5">
					<div class="panel panel-info">
						<div class="panel-heading"><h1>人均消费</h1>(本期)</div>
						<div class="panel-body">
							<h1 class="average_h1"></h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row tm-content-row tm-mt-small">
				<div class="col-xl-7 col-lg-12 tm-md-12 tm-sm-12 tm-col">
                    <div class="bg-white tm-block h-100">
                    	<div class="row">
							<div class="col-md-8 col-sm-12">
								<h2 class="tm-block-title d-inline-block">本期账单明细</h2>
							</div>
							<div class="col-md-4 col-sm-12 text-right">
								<button type="button" class="btn btn-primary btn-sm settle_btn">结算</button>
							</div>
						</div>
                        <div class="table-responsive">
							<table
								class="table table-hover table-striped tm-table-striped-even mt-3">
								<thead>
									<tr class="tm-bg-gray">
										<th scope="col">序号</th>
										<th scope="col" class="text-center">支出人</th>
										<th scope="col" class="text-center">收入人</th>
										<th scope="col" class="text-center">金额</th>
									</tr>
								</thead>
								<tbody class="table-tbody-class-sett"></tbody>
							</table>
						</div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-12 tm-md-12 tm-sm-12 tm-col">
                	<div class="bg-white tm-block h-100">
                		<h2 class="tm-block-title">本期实际消费</h2>
	                     <ol class="tm-list-group list_real_class"></ol>
                	</div>
                </div>
            </div>
			<!-- row -->
			<div class="row tm-content-row tm-mt-small">
				<div class="col-xl-12 col-lg-12 tm-md-12 tm-sm-12 tm-col">
					<div class="bg-white tm-block h-100">
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<h2 class="tm-block-title d-inline-block">往期账单</h2>
							</div>
						</div>
						<div class="table-responsive">
							<table
								class="table table-hover table-striped tm-table-striped-even mt-3">
								<thead>
									<tr class="tm-bg-gray">
										<th scope="col" hidden>ID</th>
										<th scope="col">序号</th>
										<th scope="col" class="text-center">结算总金额</th>
										<th scope="col" class="text-center">结算时间</th>
										<th scope="col">操作</th>
									</tr>
								</thead>
								<tbody class="table-tbody-class"></tbody>
							</table>
						</div>
						<div class="tm-table-mt tm-table-actions-row">
							<div class="tm-table-actions-col-right">
								<div>
									<div id="Pagination" class="pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 记一笔-模态框（Modal） -->
	<div class="modal fade" id="detailModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true" style="top: 60px;">
		<div class="modal-dialog" style="max-width: 70%;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">详情</h4>
				</div>
				<div class="modal-body">
					<div class="row tm-content-row tm-mt-small">
						<div class="col-xl-7 col-lg-12 tm-md-12 tm-sm-12 tm-col">
		                    <div class="bg-white tm-block h-100">
		                    	<div class="row">
									<div class="col-md-8 col-sm-12">
										<h2 class="tm-block-title d-inline-block">账单详情</h2>
									</div>
								</div>
		                        <div class="table-responsive">
									<table
										class="table table-hover table-striped tm-table-striped-even mt-3">
										<thead>
											<tr class="tm-bg-gray">
												<th scope="col">序号</th>
												<th scope="col" class="text-center">姓名</th>
												<th scope="col" class="text-center">结算金额</th>
											</tr>
										</thead>
										<tbody class="table-tbody-class-model-1"></tbody>
									</table>
								</div>
		                    </div>
		                </div>
		                <div class="col-xl-5 col-lg-12 tm-md-12 tm-sm-12 tm-col">
		                	<div class="bg-white tm-block h-100">
		                    	<div class="row">
									<div class="col-md-8 col-sm-12">
										<h2 class="tm-block-title d-inline-block">账单明细</h2>
									</div>
								</div>
		                        <div class="table-responsive">
									<table
										class="table table-hover table-striped tm-table-striped-even mt-3">
										<thead>
											<tr class="tm-bg-gray">
												<th scope="col">序号</th>
												<th scope="col" class="text-center">支出人</th>
												<th scope="col" class="text-center">收入人</th>
												<th scope="col" class="text-center">金额</th>
											</tr>
										</thead>
										<tbody class="table-tbody-class-model-2"></tbody>
									</table>
								</div>
		                    </div>
		                </div>
		            </div>
				
					<!-- <div class="table-responsive">
						<table
							class="table table-hover table-striped tm-table-striped-even mt-3">
							<thead>
								<tr class="tm-bg-gray">
									<th scope="col">序号</th>
									<th scope="col" class="text-center">姓名</th>
									<th scope="col" class="text-center">结算金额</th>
								</tr>
							</thead>
							<tbody class="table-tbody-class-detail"></tbody>
						</table>
					</div> -->
				</div>
				<div class="modal-footer"></div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.selection.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.pagination.js" charset="UTF-8"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/underscore-min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap-datetimepicker.zh-CN.js"
		charset="UTF-8"></script>

	<script type="text/javascript">
		var pageSize = 10;//每页显示多少条记录
	    var total;
		
	    $(function () {
	    	// 初始化列表
	    	calllist(1);
	    	
	    	// 初始化数据
	    	initAmountToBeSettled();
	    	
	    	// 初始化本期详情之明细数据
	    	initUserToUserOutMoney();
	    	
	    	// 初始化分页插件
        	$("#Pagination").pagination({
    			pageCount: total,
    			callback: function(index){
    				calllist(index.getCurrent());
    			},
    			jump: true, // 跳转到指定页数
    			jumpBtn: '跳转', // 跳转按钮文本
    			coping: false, // 首页和尾页
    			isHide: true, // 当前页数为0页或者1页时不显示分页
    			prevContent: ' 上一页 ',
    	        nextContent: ' 下一页 ',
    			items_per_page: pageSize,
    			num_display_entries: 0, // 连续分页主体部分显示的分页条目数
    			num_edge_entries: 0, // 两侧显示的首尾分页的条目数
    		})
	    });
	    
	    
	    /*查询列表*/
        function calllist(pageIndex){
        	var object = {
     			"pageNumber" : pageIndex,
     			"pageSize" : pageSize
   			};
            $.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/settle/listSettle',
  				type : 'POST',
  				data : JSON.stringify(object),
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data.data;
  						total = Math.ceil(data.data.total / pageSize); // 当前栏目下的总数
  						var tbody='';
  						var index = 1;
  	  					for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td hidden>' + list[i].id + '</td>';
  							tbody += '<td>' + index + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].settleTotalMoney + '元</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].createTime + '</td>';
  	  	  					tbody += '<td><i class="tm-trash-icon detail">详情</i></td>';
  	  	  					tbody += '</tr>';
  	  	  					index ++ ;
  						}
  		  				//加载数据到页面的 <tbody>标签中
  		  				$('.table-tbody-class')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
        }
	    
        /*查询待结算总金额*/
	    function initAmountToBeSettled() {
	    	$.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/settle/countAmountToBeSettled',
  				type : 'POST',
  				async: true,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var result = data.data;
  						$('.total_h1').text('￥' + result.total);
  						$('.average_h1').text('￥' + result.average);
  						var tbody='';
  						var values = result.values;
  						for ( var i in values) {
  							tbody += '<li class="tm-list-group-item">' + result.values[i] + '</li>';
						}
  						$('.list_real_class')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
		}
        
        /* 初始化本期详情明细列表*/
	    function initUserToUserOutMoney() {
        	$.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/settle/userToUserOutMoney',
  				type : 'POST',
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data;
  						var tbody='';
  						var index = 1;
  	  					for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td>' + index + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].sendUserName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].receiveUserName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].money + '</td>';
  	  	  					tbody += '</tr>';
  	  	  					index ++ ;
  						}
  		  				//加载数据到页面的 <tbody>标签中
  		  				$('.table-tbody-class-sett')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
		}
	    
        /*详情*/
        $('.table-tbody-class').on('click', '.detail', function(){
        	var group_id = $(this).parent().parent().find('td').eq(0).text();
            
         	// 初始化详情列表
	    	callhistorylist(group_id);
         	
         	// 初始化详情明细列表
	    	calldetaillist(group_id);
	    	
            $('#detailModal').modal('show');
        });
        
        /*查询详情列表*/
        function callhistorylist(settleId){
        	var object = {
     			"settleId" : settleId
   			};
            $.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/settle/listSettleHistory',
  				type : 'POST',
  				data : JSON.stringify(object),
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data.data;
  						var tbody='';
  						var index = 1;
  	  					for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td>' + index + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].realName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].settleMoney + '元</td>';
  	  	  					tbody += '</tr>';
  	  	  					index ++ ;
  						}
  		  				//加载数据到页面的 <tbody>标签中
  		  				$('.table-tbody-class-model-1')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
        }
        
        /*查询详情明细列表*/
        function calldetaillist(settleId){
        	var object = {
     			"settleId" : settleId
   			};
            $.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/settle/listSettleHistoryDetail',
  				type : 'POST',
  				data : JSON.stringify(object),
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						var list = data.data;
  						var tbody='';
  						var index = 1;
  	  					for ( var i in list) {
  							tbody +='<tr>';
  							tbody += '<td>' + index + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].sendUserName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].receiveUserName + '</td>';
  	  	  					tbody += '<td class="text-center">' + list[i].money + '元</td>';
  	  	  					tbody += '</tr>';
  	  	  					index ++ ;
  						}
  		  				//加载数据到页面的 <tbody>标签中
  		  				$('.table-tbody-class-model-2')[0].innerHTML = tbody;
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
        }
        
        /* 执行结算*/
        $('.settle_btn').click(function(){
        	$.ajax({
            	contentType: "application/json;charset-UTF-8",
  			  	url : '<%=request.getContextPath()%>/settle/settle',
  				type : 'POST',
  				async: false,
  				dataType : "json",
  				success : function(data) {
  					if (data.errCode == 200) {
  						alert(JSON.stringify(data));
  						location.reload();
					} else {
						alert(data.errMsg);
					}
  				},
  				error : function(data) {
  					alert(data);
  				}
  			});
        });
	</script>
</body>
</html>