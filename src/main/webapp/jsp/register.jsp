<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
    String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                    + path + "/";
            request.setAttribute("basePath", basePath);
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>米缘.记一笔</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/fontawesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/tooplate.css">
</head>
<body>
    <div class="container">
        <div class="row tm-mt-big">
            <div class="col-12 mx-auto tm-login-col">
                <div class="bg-white tm-block">
                    <div class="row">
                        <div class="col-12 text-center">
                            <i class="fas fa-3x fa-tachometer-alt tm-site-icon text-center"></i>
                            <h2 class="tm-block-title mt-3">Apply for MiYuan account</h2>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <form action="#" method="post" class="tm-register-form">
                                <div class="input-group">
                                    <label for="userName" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Username</label>
                                    <input name="userName" type="text" class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7" id="userName">
                                </div>
                                <div class="input-group mt-3">
                                    <label for="passWord" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Password</label>
                                    <input name="passWord" type="password" class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7" id="passWord">
                                </div>
                                <div class="input-group mt-3">
                                    <label for="realName" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Realname</label>
                                    <input name="realName" type="text" class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7" id="realName">
                                </div>
                                <div class="input-group mt-3">
                                    <label for="userAddress" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">UserAddress</label>
                                    <input name="userAddress" type="text" class="form-control validate" id="userAddress">
                                </div>
                                <div class="input-group mt-3">
                                    <button type="button" class="btn btn-primary d-inline-block mx-auto register_btn">Register</button>
                                    <button type="button" class="btn btn-primary d-inline-block mx-auto back">Back</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Javascript -->
	<script src="<%=request.getContextPath()%>/bootstrap/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		/*点击注册按钮*/
		$('.register_btn').click(function(){
			$.ajax({
				url : '<%=basePath%>userinfo/add',
				type : 'POST',
				data : JSON.stringify($('.tm-register-form').serializeObject()),
				async: false,
				dataType: "json",
				contentType:"application/json",  //缺失会出现URL编码，无法转成json对象
				success: function (data) {
					if (data.errCode == 200) {
						window.location.href = "<%=basePath%>tologin";
	            	} else {
	            		alert(data.errMsg + ":" +JSON.stringify(data.data));
	            	}
	            },
			    error: function (data) {
			    	alert(JSON.stringify(data));
            	  	alert(data.msg);
                }
          });
		});
		
		/*返回*/
		$('.back').click(function(){
			window.location.href = "<%=basePath%>tologin";
		});
		
		/**
	     * 自动将form表单封装成json对象
	     */
	    $.fn.serializeObject = function() {
	        var o = {};
	        var a = this.serializeArray();
	        $.each(a, function() {
	            if (o[this.name]) {
	                if (!o[this.name].push) {
	                    o[this.name] = [ o[this.name] ];
	                }
	                o[this.name].push(this.value || '');
	            } else {
	                o[this.name] = this.value || '';
	            }
	        });
	        return o;
	    };
	</script>
</body>
</html>