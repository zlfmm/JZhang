package com.miyuan.jizhang.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.response.BaseResult;

public class MiscUtils {

	static public BaseResult<Map<String, String>> getValidateError(BindingResult bindingResult) {
		if (bindingResult.hasErrors() == false)
			return null;
		Map<String, String> fieldErrors = new HashMap<String, String>();
		for (FieldError error : bindingResult.getFieldErrors()) {
			fieldErrors.put(error.getField(), error.getDefaultMessage());
		}
		BaseResult<Map<String, String>> ret = new BaseResult<Map<String, String>>(
				ExceptionCodeEnum.REQUEST_PARAM_ERROR);
		ret.setData(fieldErrors);
		return ret;
	}

}
