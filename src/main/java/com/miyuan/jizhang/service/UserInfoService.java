package com.miyuan.jizhang.service;

import java.util.List;

import com.miyuan.jizhang.request.DeleteUserInfoRequestVo;
import com.miyuan.jizhang.request.InsertUserInfoRequestVo;
import com.miyuan.jizhang.request.ListUserInfoRequestVo;
import com.miyuan.jizhang.request.UpdateUserInfoRequestVo;
import com.miyuan.jizhang.response.ListUserInfoResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;

/**
 * 用户操作业务类接口
 * 
 * @author zhanglongfei
 *
 */
public interface UserInfoService {

	/**
	 * 新增用户信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean addUserInfo(InsertUserInfoRequestVo vo);

	/**
	 * 查询用户信息列表
	 * 
	 * @param vo
	 * @return
	 */
	public PageResult<ListUserInfoResponseVo> listUserInfo(ListUserInfoRequestVo vo);

	/**
	 * 根据用户ID修改用户信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateUserInfoById(UpdateUserInfoRequestVo vo);

	/**
	 * 根据用户ID删除用户信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean deleteUserInfoById(DeleteUserInfoRequestVo vo);
	
	/**
	 * 登录
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public LoginUserInfoResponseVo login(String username, String password);
	
	/**
	 * 根据邮箱模糊查询用户信息
	 * 
	 * @param email
	 * @return
	 */
	public List<ListUserInfoResponseVo> listUserInfoByEmail(String email);

}
