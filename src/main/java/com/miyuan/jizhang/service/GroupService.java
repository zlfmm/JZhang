package com.miyuan.jizhang.service;

import java.util.List;

import com.miyuan.jizhang.entity.UserGroupPo;
import com.miyuan.jizhang.request.DeleteGroupRequestVo;
import com.miyuan.jizhang.request.InsertGroupRequestVo;
import com.miyuan.jizhang.request.ListGroupRequestVo;
import com.miyuan.jizhang.request.UpdateGroupRequestVo;
import com.miyuan.jizhang.response.ListGroupResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;

/**
 * 群操作业务类接口
 * 
 * @author zhanglongfei
 *
 */
public interface GroupService {

	/**
	 * 新增群信息
	 * 
	 * @param vo
	 * @param user
	 * @return
	 */
	public boolean addGroup(InsertGroupRequestVo vo, LoginUserInfoResponseVo user);

	/**
	 * 查询群信息列表
	 * 
	 * @param vo
	 * @param user
	 * @return
	 */
	public PageResult<ListGroupResponseVo> listUserInfo(ListGroupRequestVo vo, LoginUserInfoResponseVo user);
	
	/**
	 * 根据用户ID修改群信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateUserInfoById(UpdateGroupRequestVo vo);

	/**
	 * 根据用户ID删除群信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean deleteUserInfoById(DeleteGroupRequestVo vo);

	public boolean addGroup111(InsertGroupRequestVo vo) throws Exception;

	/**
	 * 查询所在群组的组成员
	 * 
	 * @return
	 */
	public List<UserGroupPo> findMembersByuserId(Integer userId);

	/**
	 * 根据用户id查询群组信息
	 * 
	 * @return
	 */
	public UserGroupPo findGroupByUserId(Integer userId);
	
	/**
	 * 邀请入群组
	 * 
	 * @param userId
	 * @param uId
	 * @return
	 */
	public boolean invitateUser(Integer userId, Integer uId);
	
	/**
	 * 解散群组
	 * 
	 * @param userId
	 * @return
	 */
	public boolean dissoluteGroup(Integer userId);
}
