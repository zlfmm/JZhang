package com.miyuan.jizhang.service;

import java.text.ParseException;
import java.util.List;

import com.miyuan.jizhang.request.DeleteMoneyByIdsRequestVo;
import com.miyuan.jizhang.request.InsertMoneyRequestVo;
import com.miyuan.jizhang.request.ListMoneyRequestVo;
import com.miyuan.jizhang.response.CountMoneyLineChartResponseVo;
import com.miyuan.jizhang.response.ListMoneyResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;

/**
 * 金额记录表操作业务
 * 
 * @author zhanglongfei
 *
 */
public interface MoneyService {

	/**
	 * 
	 * 新增消费记录信息
	 * 
	 * @param vo
	 * @return
	 * @throws ParseException
	 */
	public boolean addMoney(InsertMoneyRequestVo vo, LoginUserInfoResponseVo userInfoResponseVo) throws Exception;

	/**
	 * 条件查询消费记录
	 * 
	 * @param vo
	 * @return
	 */
	public PageResult<ListMoneyResponseVo> listMoney(ListMoneyRequestVo vo);

	/**
	 * 根据id集合结算消费记录
	 * 
	 * @param ids
	 * @return
	 */
	public boolean settleMoneyByIds(List<Integer> ids);

	/**
	 * 根据id集合删除消费记录信息
	 * 
	 * @param curUser
	 * @param vo
	 * @return
	 */
	public boolean deleteMoneyByIds(LoginUserInfoResponseVo curUser, DeleteMoneyByIdsRequestVo vo);
	
	/**
	 * 统计本期消费金额折线图数据
	 * 
	 * @param curUser
	 * @return
	 */
	public List<CountMoneyLineChartResponseVo> countMoneyLineChart(LoginUserInfoResponseVo curUser);
	
	/**
	 * 统计本期每个组员消费金额柱状图数据
	 * 
	 * @param curUser
	 * @return
	 */
	public List<CountMoneyLineChartResponseVo> countMoneyBarChart(LoginUserInfoResponseVo curUser);
	
	/**
	 * 统计本期每个组员消费金额饼状图数据
	 * 
	 * @param curUser
	 * @return
	 */
	public List<CountMoneyLineChartResponseVo> countMoneyPieChart(LoginUserInfoResponseVo curUser);

}
