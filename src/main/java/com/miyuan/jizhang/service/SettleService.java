package com.miyuan.jizhang.service;

import java.util.List;

import com.miyuan.jizhang.request.BaseRequest;
import com.miyuan.jizhang.request.ListSettleHistoryRequestVo;
import com.miyuan.jizhang.response.CountAmountToBeSettledResponseVo;
import com.miyuan.jizhang.response.GetUserToUserOutMoneyResponseVo;
import com.miyuan.jizhang.response.ListSettleHistoryResponseVo;
import com.miyuan.jizhang.response.ListSettleResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;

/**
 * 结算历史表操作业务
 * 
 * @author zhanglongfei
 *
 */
public interface SettleService {
	
	/**
	 * 分页查询消费主表记录
	 * 
	 * @param vo
	 * @param user
	 * @return
	 */
	public PageResult<ListSettleResponseVo> listSettle(BaseRequest vo, LoginUserInfoResponseVo user);

	/**
	 * 分页查询消费详情记录
	 * 
	 * @param vo
	 * @param user
	 * @return
	 */
	public PageResult<ListSettleHistoryResponseVo> listSettleHistory(ListSettleHistoryRequestVo vo, LoginUserInfoResponseVo user);
	
	/**
	 * 分页查询消费详情明细记录
	 * 
	 * @param vo
	 * @return
	 */
	List<GetUserToUserOutMoneyResponseVo> listSettleHistoryDetail(ListSettleHistoryRequestVo vo);

	/**
	 * 待结算总金额
	 * 
	 * @param cUser
	 * @return
	 */
	public CountAmountToBeSettledResponseVo countAmountToBeSettled(LoginUserInfoResponseVo cUser);
	
	/**
	 * 结算
	 * 
	 * @param userId
	 */
	public void settle(Integer userId);
	
	/**
	 * 查询本期结算详情之明细信息
	 * 
	 * @param userId
	 * @return
	 */
	public List<GetUserToUserOutMoneyResponseVo> userToUserOutMoney(Integer userId);

}
