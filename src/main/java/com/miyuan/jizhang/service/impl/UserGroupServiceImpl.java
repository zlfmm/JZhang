package com.miyuan.jizhang.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.miyuan.jizhang.entity.UserGroupPo;
import com.miyuan.jizhang.mapper.UserGroupMapper;
import com.miyuan.jizhang.service.UserGroupService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserGroupServiceImpl extends ServiceImpl<UserGroupMapper, UserGroupPo> implements UserGroupService {
	
	@Autowired
	private UserGroupMapper userGroupMapper;

}
