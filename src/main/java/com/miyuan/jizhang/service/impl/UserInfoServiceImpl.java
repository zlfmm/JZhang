package com.miyuan.jizhang.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.miyuan.jizhang.entity.UserInfoPo;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.UserInfoException;
import com.miyuan.jizhang.mapper.UserInfoMapper;
import com.miyuan.jizhang.request.DeleteUserInfoRequestVo;
import com.miyuan.jizhang.request.InsertUserInfoRequestVo;
import com.miyuan.jizhang.request.ListUserInfoRequestVo;
import com.miyuan.jizhang.request.UpdateUserInfoRequestVo;
import com.miyuan.jizhang.response.ListUserInfoResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.UserInfoService;
import com.miyuan.jizhang.utils.BeanUtils;
import com.miyuan.jizhang.utils.Md5Utils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfoPo> implements UserInfoService {

	@Autowired
	private UserInfoMapper userInfoMapper;

	@Override
	public boolean addUserInfo(InsertUserInfoRequestVo vo) {
		// 校验邮箱是否已注册
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("jz_user_address", vo.getUserAddress());
		List<UserInfoPo> checkAddresses = userInfoMapper.selectByMap(columnMap);
		if (!CollectionUtils.isEmpty(checkAddresses)) {
			throw new UserInfoException(ExceptionCodeEnum.ADDRESS_ALREADY_INUSE);
		}
		
		// 校验用户名是否已存在
		columnMap = new HashMap<String, Object>();
		columnMap.put("jz_user_name", vo.getUserName());
		List<UserInfoPo> checkUserNames = userInfoMapper.selectByMap(columnMap);
		if (!CollectionUtils.isEmpty(checkUserNames)) {
			throw new UserInfoException(ExceptionCodeEnum.USER_NAME_ALREADY_INUSE);
		}
		
		String encryptPassword = Md5Utils.encryptPassword(vo.getPassWord(), Md5Utils.salt);
		vo.setPassWord(encryptPassword);
		UserInfoPo po = BeanUtils.convert(vo, UserInfoPo.class);
		Integer insert = userInfoMapper.insert(po);
		log.info("新增用户信息返回值：[{}]", insert);
		if (insert > 0) {
			return true;
		}
		return false;
	}

	@Override
	public PageResult<ListUserInfoResponseVo> listUserInfo(ListUserInfoRequestVo vo) {
		Page<UserInfoPo> page = new Page<UserInfoPo>(vo.getPageNumber(), vo.getPageSize());
		EntityWrapper<UserInfoPo> wrapper = new EntityWrapper<UserInfoPo>();
		wrapper.eq("jz_del", 0);
		if (vo.getId() != null) {
			wrapper.eq("jz_id", vo.getId());
		}
		if (StringUtils.isNoneBlank(vo.getRealName())) {
			wrapper.eq("jz_real_name", vo.getRealName());
		}
		if (StringUtils.isNoneBlank(vo.getUserName())) {
			wrapper.eq("jz_user_name", vo.getUserName());
		}
		if (StringUtils.isNoneBlank(vo.getPassWord())) {
			wrapper.eq("jz_pass_word", vo.getPassWord());
		}
		List<UserInfoPo> list = userInfoMapper.selectPage(page, wrapper);
		List<ListUserInfoResponseVo> data = BeanUtils.convert(list, ListUserInfoResponseVo.class);
		PageResult<ListUserInfoResponseVo> pageResult = new PageResult<ListUserInfoResponseVo>(data, vo.getPageNumber(),
				vo.getPageSize(), page.getTotal());
		log.info("查询用户信息列表返回值：[{}]", pageResult.toString());
		return pageResult;
	}

	@Override
	public boolean updateUserInfoById(UpdateUserInfoRequestVo vo) {
		UserInfoPo entity = userInfoMapper.selectById(vo.getId());
		Integer updateById = 0;
		if (entity != null) {
			if (StringUtils.isNotBlank(vo.getPassWord())) {
				// 密码加密（对base64加密的字符串进行md5加密）
				vo.setPassWord(Md5Utils.encryptPassword(vo.getPassWord(), Md5Utils.salt));
			}
			BeanUtils.copyProperties(vo, entity);
			updateById = userInfoMapper.updateById(entity);
			log.info("修改用户信息返回值：[{}]", updateById);
		}
		if (updateById > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteUserInfoById(DeleteUserInfoRequestVo vo) {
		try {
			if (!CollectionUtils.isEmpty(vo.getIds())) {
				Integer updateUserInfoStatusIsDel = userInfoMapper.updateUserInfoStatusIsDel(vo.getIds());
				log.info("删除用户信息返回值：[{}]", updateUserInfoStatusIsDel);
			}
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
	}

	@Override
	public LoginUserInfoResponseVo login(String username, String password) {
		// 密码加密（对base64加密的字符串进行md5加密）
		password = new String(Md5Utils.encryptPassword(password, Md5Utils.salt));
		// 根据用户名密码查询
		Wrapper<UserInfoPo> wrapper = new EntityWrapper<UserInfoPo>();
		wrapper.orNew().eq("jz_user_address", username).orNew().eq("jz_user_name", username);
		wrapper.andNew().eq("jz_pass_word", password);
		List<UserInfoPo> list = userInfoMapper.selectList(wrapper);
		// 查询到了数据就返回，说明登录成功
		if (!CollectionUtils.isEmpty(list)) {
			return BeanUtils.convert(list.get(0), LoginUserInfoResponseVo.class);
		}
		// 判断登录失败的原因
		UserInfoPo entity = new UserInfoPo();
		entity.setUserName(username);
		UserInfoPo selectByUserName = userInfoMapper.selectOne(entity);
		if (selectByUserName == null) {
			throw new UserInfoException(ExceptionCodeEnum.USER_NAME_INPUT_ERROR);
		}
		if (!selectByUserName.getPassWord().equals(password)) {
			throw new UserInfoException(ExceptionCodeEnum.PASS_WORD_INPUT_ERROR);
		}
		return null;
	}

	@Override
	public List<ListUserInfoResponseVo> listUserInfoByEmail(String email) {
		Wrapper<UserInfoPo> wrapper = new EntityWrapper<UserInfoPo>();
		wrapper.like("jz_user_address", email);
		wrapper.orderBy("jz_create_time", false);
		wrapper.eq("jz_del", 0);
		List<UserInfoPo> userInfoPos = userInfoMapper.selectList(wrapper);
		return BeanUtils.convert(userInfoPos, ListUserInfoResponseVo.class);
	}

}
