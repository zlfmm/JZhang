package com.miyuan.jizhang.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.miyuan.jizhang.constant.JizhangConstant;
import com.miyuan.jizhang.entity.MoneyPo;
import com.miyuan.jizhang.entity.UserGroupPo;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.MoneyException;
import com.miyuan.jizhang.mapper.MoneyMapper;
import com.miyuan.jizhang.mapper.UserGroupMapper;
import com.miyuan.jizhang.request.DeleteMoneyByIdsRequestVo;
import com.miyuan.jizhang.request.InsertMoneyRequestVo;
import com.miyuan.jizhang.request.ListMoneyRequestVo;
import com.miyuan.jizhang.response.CountMoneyLineChartResponseVo;
import com.miyuan.jizhang.response.ListMoneyResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.GroupService;
import com.miyuan.jizhang.service.MoneyService;
import com.miyuan.jizhang.utils.BeanUtils;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MoneyServiceImpl extends ServiceImpl<MoneyMapper, MoneyPo> implements MoneyService {

	@Autowired
	private MoneyMapper moneyMapper;

	@Autowired
	private UserGroupMapper userGroupMapper;
	
	@Autowired
	private GroupService groupService;

	/**
	 * 根据当前登录用户id和群组id获取到用户和组的关联表id，然后新增进此次消费记录里面
	 * @param vo
	 * @param userInfoResponseVo
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean addMoney(InsertMoneyRequestVo vo, LoginUserInfoResponseVo userInfoResponseVo) throws Exception {
		log.info("请求参数:[{}]", JSON.toJSONString(vo));
		if (StringUtils.isNoneBlank(vo.getDesc()) && vo.getDesc().length() > 100) {
			throw new MoneyException(ExceptionCodeEnum.LONG_DETAIL_INSERT_ERROR);
		}
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("jz_user_id", userInfoResponseVo.getId());
		columnMap.put("jz_group_id", vo.getUserGroupId());
		columnMap.put("jz_del", 0);
		List<UserGroupPo> userGroupPos = userGroupMapper.selectByMap(columnMap);
		if (!CollectionUtils.isEmpty(userGroupPos)) {
			vo.setUserGroupId(userGroupPos.get(0).getId());
		}
		MoneyPo entity = BeanUtils.convert(vo, MoneyPo.class);
		Integer insert = moneyMapper.insert(entity);
		log.info("新增消费记录返回值:[{}]", insert);
		if (insert > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 根据群组id获取级联id，然后查询消费记录表
	 */
	@Override
	public PageResult<ListMoneyResponseVo> listMoney(ListMoneyRequestVo vo) {
		log.info("请求参数:[{}]", JSON.toJSONString(vo));
		if (Objects.isNull(vo.getUserId())) {
			throw new MoneyException(ExceptionCodeEnum.NOT_LOGGED_IN_YET);
		}
		// 根据用户查询，该用户所在组的用户和组关联表Id
		List<UserGroupPo> userGroupPos_ = groupService.findMembersByuserId(vo.getUserId());
		List<Integer> userGroupIds = null;
		if (CollectionUtils.isNotEmpty(userGroupPos_)) {
			userGroupIds = new ArrayList<Integer>();
			for (UserGroupPo userGroupPo_ : userGroupPos_) {
				userGroupIds.add(userGroupPo_.getId());
			}
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new MoneyException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		// 条件查询记账记录列表
		Page<MoneyPo> page = new Page<MoneyPo>(vo.getPageNumber(), vo.getPageSize());
		Wrapper<MoneyPo> wrapper = getMoneyPoWrapper(vo, userGroupIds);
		List<MoneyPo> list = moneyMapper.selectPage(page, wrapper);
		List<Integer> userGroupIdsRes = new ArrayList<Integer>();
		for (MoneyPo moneyPo : list) {
			userGroupIdsRes.add(moneyPo.getUserGroupId());
		}
		List<UserGroupPo> selectBatchIds = new ArrayList<UserGroupPo>();
		if (!CollectionUtils.isEmpty(userGroupIdsRes)) {
			selectBatchIds = userGroupMapper.selectBatchIds(userGroupIdsRes);
		}
		Map<Integer, String> map = new HashMap<Integer, String>();
		for (UserGroupPo userGroupEntity : selectBatchIds) {
			map.put(userGroupEntity.getId(), userGroupEntity.getRealName());
		}
		List<ListMoneyResponseVo> data = BeanUtils.convert(list, ListMoneyResponseVo.class);
		for (ListMoneyResponseVo listMoneyResponseVo : data) {
			if (map.containsKey(listMoneyResponseVo.getUserGroupId())) {
				listMoneyResponseVo.setRealName(map.get(listMoneyResponseVo.getUserGroupId()));
			}
		}
		PageResult<ListMoneyResponseVo> pageResult = new PageResult<ListMoneyResponseVo>(data, vo.getPageNumber(),
				vo.getPageSize(), page.getTotal());
		return pageResult;
	}
	
	/*
	 * 获取查询条件Wrapper
	 */
	private Wrapper<MoneyPo> getMoneyPoWrapper(ListMoneyRequestVo vo, List<Integer> userGroupIds) {
		Wrapper<MoneyPo> wrapper = new EntityWrapper<MoneyPo>();
		if (StringUtils.isNoneBlank(vo.getStartTime())) {
			wrapper.gt("jz_settle_time", vo.getStartTime());
		}
		if (StringUtils.isNoneBlank(vo.getEndTime())) {
			wrapper.lt("jz_settle_time", vo.getEndTime());
		}
		if (StringUtils.isNoneBlank(vo.getPayStartTime())) {
			wrapper.gt("jz_pay_time", vo.getPayStartTime());
		}
		if (StringUtils.isNoneBlank(vo.getPayEndTime())) {
			wrapper.lt("jz_pay_time", vo.getPayEndTime());
		}
		if (!CollectionUtils.isEmpty(userGroupIds)) {
			wrapper.in("jz_user_group_id", userGroupIds);
		}
		if (vo.getIsSettle() != null) {
			wrapper.eq("jz_is_settle", vo.getIsSettle());
		}
		if (vo.getIsDel() != null) {
			wrapper.eq("jz_del", vo.getIsDel());
		}
		wrapper.orderBy("jz_create_time", false);
		return wrapper;
	}

	@Override
	public boolean settleMoneyByIds(List<Integer> ids) {
		try {
			if (!CollectionUtils.isEmpty(ids)) {
				moneyMapper.updateIsSettleById(ids, JizhangConstant.issettle_1);
			}
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
	}

	/**
	 * 根据id集合删除消费记录信息
	 */
	@Override
	@Synchronized
	public boolean deleteMoneyByIds(LoginUserInfoResponseVo curUser, DeleteMoneyByIdsRequestVo vo) {
		Wrapper<UserGroupPo> wrapper = new EntityWrapper<UserGroupPo>();
		wrapper.eq("jz_user_id", curUser.getId());
		wrapper.eq("jz_del", 0);
		List<UserGroupPo> userGroupPos = userGroupMapper.selectList(wrapper);
		UserGroupPo userGroupPo = null;
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			userGroupPo = userGroupPos.get(0);
		}
		if (Objects.isNull(userGroupPo)) {
			throw new MoneyException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		Wrapper<MoneyPo> wrapperMoney = new EntityWrapper<MoneyPo>();
		wrapperMoney.eq("jz_user_group_id", userGroupPo.getId());
		wrapperMoney.eq("jz_del", 0);
		List<MoneyPo> moneyPos = moneyMapper.selectList(wrapperMoney);
		List<Integer> moneyIds = null;
		if (CollectionUtils.isNotEmpty(moneyPos)) {
			moneyIds = new ArrayList<Integer>(moneyPos.size());
			Iterator<MoneyPo> iterator = moneyPos.iterator();
			while (iterator.hasNext()) {
				MoneyPo moneyPo = (MoneyPo) iterator.next();
				moneyIds.add(moneyPo.getId());
			}
		}
		if (CollectionUtils.isEmpty(moneyIds)) {
			throw new MoneyException(ExceptionCodeEnum.NOT_FIND_YOUR_PAY_HISTORY_ERROR);
		}
		if (CollectionUtils.isEmpty(vo.getIds())) {
			throw new MoneyException(ExceptionCodeEnum.REQUEST_PARAM_ERROR);
		}
		if (moneyIds.containsAll(vo.getIds())) {
			Integer updateUserInfoStatusIsDel = moneyMapper.updateMoneyStatusIsDel(vo.getIds());
			log.info("删除消费记录信息返回值：[{}]", updateUserInfoStatusIsDel);
		} else {
			throw new MoneyException(ExceptionCodeEnum.DELETE_ONLY_YOURSELF_PAY_ERROR);
		}
		return true;
	}

	@Override
	public List<CountMoneyLineChartResponseVo> countMoneyLineChart(LoginUserInfoResponseVo curUser) {
		// 根据用户查询，该用户所在组的用户和组关联表Id
		List<UserGroupPo> userGroupPos_ = groupService.findMembersByuserId(curUser.getId());
		List<Integer> userGroupIds = null;
		if (CollectionUtils.isNotEmpty(userGroupPos_)) {
			userGroupIds = new ArrayList<>(userGroupPos_.size());
			Iterator<UserGroupPo> iterator = userGroupPos_.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				userGroupIds.add(userGroupPo.getId());
			}
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new MoneyException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		return moneyMapper.countMoneyLineChart(userGroupIds);
	}

	@Override
	public List<CountMoneyLineChartResponseVo> countMoneyBarChart(LoginUserInfoResponseVo curUser) {
		// 根据用户查询，该用户所在组的用户和组关联表Id
		List<UserGroupPo> userGroupPos_ = groupService.findMembersByuserId(curUser.getId());
		List<Integer> userGroupIds = null;
		if (CollectionUtils.isNotEmpty(userGroupPos_)) {
			userGroupIds = new ArrayList<>(userGroupPos_.size());
			Iterator<UserGroupPo> iterator = userGroupPos_.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				userGroupIds.add(userGroupPo.getId());
			}
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new MoneyException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		return moneyMapper.countMoneyBarChart(userGroupIds);
	}
	
	@Override
	public List<CountMoneyLineChartResponseVo> countMoneyPieChart(LoginUserInfoResponseVo curUser) {
		// 根据用户查询，该用户所在组的用户和组关联表Id
		List<UserGroupPo> userGroupPos_ = groupService.findMembersByuserId(curUser.getId());
		List<Integer> userGroupIds = null;
		if (CollectionUtils.isNotEmpty(userGroupPos_)) {
			userGroupIds = new ArrayList<>(userGroupPos_.size());
			Iterator<UserGroupPo> iterator = userGroupPos_.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				userGroupIds.add(userGroupPo.getId());
			}
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new MoneyException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		return moneyMapper.countMoneyBarChart(userGroupIds);
	}
	

}
