package com.miyuan.jizhang.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.miyuan.jizhang.entity.GroupPo;
import com.miyuan.jizhang.entity.UserGroupPo;
import com.miyuan.jizhang.entity.UserInfoPo;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.GroupException;
import com.miyuan.jizhang.mapper.GroupMapper;
import com.miyuan.jizhang.mapper.UserGroupMapper;
import com.miyuan.jizhang.mapper.UserInfoMapper;
import com.miyuan.jizhang.request.DeleteGroupRequestVo;
import com.miyuan.jizhang.request.InsertGroupRequestVo;
import com.miyuan.jizhang.request.ListGroupRequestVo;
import com.miyuan.jizhang.request.UpdateGroupRequestVo;
import com.miyuan.jizhang.response.ListGroupResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.GroupService;
import com.miyuan.jizhang.utils.BeanUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GroupServiceImpl extends ServiceImpl<GroupMapper, GroupPo> implements GroupService {

	@Autowired
	private GroupMapper groupMapper;

	@Autowired
	private UserGroupMapper userGroupMapper;
	
	@Autowired
	private UserInfoMapper userInfoMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean addGroup(InsertGroupRequestVo vo, LoginUserInfoResponseVo user) {
		// 校验是否该用户已存在分组
		Wrapper<UserGroupPo> wrapper = new EntityWrapper<UserGroupPo>();
		wrapper.eq("jz_user_id", user.getId());
		wrapper.eq("jz_del", 0);
		List<UserGroupPo> userGroups = userGroupMapper.selectList(wrapper);
		if (CollectionUtils.isNotEmpty(userGroups)) {
			throw new GroupException(ExceptionCodeEnum.ALREADY_IN_GROUP_ERROR);
		}
		// 校验重名
		Iterator<UserGroupPo> iterator = userGroups.iterator();
		while (iterator.hasNext()) {
			UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
			if (userGroupPo.getGroupName().equals(vo.getName())) {
				throw new GroupException(ExceptionCodeEnum.CHECK_GROUP_IS_IN_USE_ERROR);
			}
		}
		// 新增分组表
		GroupPo entity = BeanUtils.convert(vo, GroupPo.class);
		entity.setCreateUser(user.getId());
		Integer insert = groupMapper.insert(entity);
		log.info("新增群组信息返回值:[{}]", insert);
		// 新增群组时，将当前用户添加到该群组
		UserGroupPo userGroupPo = new UserGroupPo(null, entity.getId(), entity.getName(), user.getId(),
				user.getRealName(), null, 0);
		userGroupMapper.insert(userGroupPo);
		if (insert > 0) {
			return true;
		}
		return false;
	}

	@Override
	public PageResult<ListGroupResponseVo> listUserInfo(ListGroupRequestVo vo, LoginUserInfoResponseVo user) {
		vo.setPageNumber(vo.getPageNumber() - 1);
		List<ListGroupResponseVo> list = groupMapper.list(vo, user.getId());
		Integer count = groupMapper.count(vo, user.getId());
		PageResult<ListGroupResponseVo> pageResult = new PageResult<ListGroupResponseVo>(list, vo.getPageNumber(),
				vo.getPageSize(), count);
		log.info("查询群信息列表返回值：[{}]", pageResult.toString());
		return pageResult;
	}

	@Override
	public boolean updateUserInfoById(UpdateGroupRequestVo vo) {
		GroupPo entity = groupMapper.selectById(vo.getId());
		Integer updateById = 0;
		if (entity != null) {
			BeanUtils.copyProperties(vo, entity);
			updateById = groupMapper.updateById(entity);
			log.info("修改群信息返回值：[{}]", updateById);
		}
		
		// 修改关联表
		userGroupMapper.updateGroupNameByGroupId(vo.getId(), vo.getName());
		
		if (updateById > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteUserInfoById(DeleteGroupRequestVo vo) {
		try {
			if (!CollectionUtils.isEmpty(vo.getIds())) {
				Integer updateGroupStatusIsDel = groupMapper.updateGroupStatusIsDel(vo.getIds());
				log.info("删除群信息返回值：[{}]", updateGroupStatusIsDel);
			}
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean addGroup111(InsertGroupRequestVo vo) throws Exception {
		GroupPo entity = BeanUtils.convert(vo, GroupPo.class);
		Integer insert = groupMapper.insert(entity);
		if (insert > 0) {
			throw new Exception("11111111");
		}
		return true;
	}

	@Override
	public List<UserGroupPo> findMembersByuserId(Integer userId) {
		// 先根据当前登录用户id查询所属分组（目前一个人只能在一个分组）
		UserGroupPo userGroupPo = null;
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("jz_user_id", userId);
		columnMap.put("jz_del", 0);
		List<UserGroupPo> userGroupPos = userGroupMapper.selectByMap(columnMap);
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			userGroupPo = userGroupPos.get(0);
		}
		// 根据分组id查询用户和分组关联表，用于查询本人能查询到的记录信息
		List<UserGroupPo> userGroupPos_ = null;
		if (Objects.nonNull(userGroupPo)) {
			Wrapper<UserGroupPo> wrapperGroup = new EntityWrapper<UserGroupPo>();
			wrapperGroup.eq("jz_group_id", userGroupPo.getGroupId());
			wrapperGroup.eq("jz_del", 0);
			userGroupPos_ = userGroupMapper.selectList(wrapperGroup);
		}
		return userGroupPos_;
	}

	@Override
	public UserGroupPo findGroupByUserId(Integer userId) {
		// 根据当前登录用户id查询所属分组（目前一个人只能在一个分组）
		UserGroupPo userGroupPo = null;
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("jz_user_id", userId);
		columnMap.put("jz_del", 0);
		List<UserGroupPo> userGroupPos = userGroupMapper.selectByMap(columnMap);
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			userGroupPo = userGroupPos.get(0);
		}
		return userGroupPo;
	}

	@Override
	public boolean invitateUser(Integer userId, Integer uId) {
		// 查询当前用户所在群组的成员
		List<UserGroupPo> userGroupPos = this.findMembersByuserId(userId);
		if (CollectionUtils.isEmpty(userGroupPos)) {
			throw new GroupException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		if (userGroupPos.size() > 8) {
			throw new GroupException(ExceptionCodeEnum.GROUP_MAST_MEMBER_ERROR);
		}
		// 查询被邀请用户所在群组
		UserGroupPo uGroupPo = this.findGroupByUserId(uId);
		if (!Objects.isNull(uGroupPo)) {
			throw new GroupException(ExceptionCodeEnum.INVITATE_USER_ALREADY_IN_GROUP_ERROR);
		}
		// 查询当前用户所在群组
		UserGroupPo userGroupPo = this.findGroupByUserId(userId);
		// 查询被邀请人的信息
		UserInfoPo userInfoPo = userInfoMapper.selectById(uId);
		// 插入数据
		UserGroupPo entity = new UserGroupPo();
		entity.setGroupId(userGroupPo.getGroupId());
		entity.setGroupName(userGroupPo.getGroupName());
		entity.setUserId(userInfoPo.getId());
		entity.setRealName(userInfoPo.getRealName());
		Integer insert = userGroupMapper.insert(entity);
		if (insert > 0) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean dissoluteGroup(Integer userId) {
		// 查询当前用户所在群组
		UserGroupPo userGroupPo = this.findGroupByUserId(userId);
		if (Objects.isNull(userGroupPo)) {
			throw new GroupException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		GroupPo groupPo01 = groupMapper.selectById(userGroupPo.getGroupId());
		if (!groupPo01.getCreateUser().equals(userId)) {
			throw new GroupException(ExceptionCodeEnum.TALK_TO_CREATE_USER_DISSOLUTE_ERROR);
		}
		// 查询当前用户所在群组的成员
		List<UserGroupPo> userGroupPos = this.findMembersByuserId(userId);
		GroupPo groupPo = groupMapper.selectById(userGroupPo.getGroupId());
		// 删除分组数据库信息
		groupPo.setIsDel(1);
		groupMapper.updateById(groupPo);
		// 删除该分组所有成员关联关系
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			Iterator<UserGroupPo> iterator = userGroupPos.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo2 = (UserGroupPo) iterator.next();
				userGroupPo2.setIsDel(1);
				userGroupMapper.updateById(userGroupPo2);
			}
		}
		return true;
	}
	
}
