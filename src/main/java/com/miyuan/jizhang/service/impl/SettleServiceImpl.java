package com.miyuan.jizhang.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.miyuan.jizhang.entity.MoneyPo;
import com.miyuan.jizhang.entity.SettleHistoryDetailPo;
import com.miyuan.jizhang.entity.SettleHistoryPo;
import com.miyuan.jizhang.entity.SettlePo;
import com.miyuan.jizhang.entity.UserGroupPo;
import com.miyuan.jizhang.entity.UserInfoPo;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.SettleException;
import com.miyuan.jizhang.mapper.MoneyMapper;
import com.miyuan.jizhang.mapper.SettleHistoryDetailMapper;
import com.miyuan.jizhang.mapper.SettleHistoryMapper;
import com.miyuan.jizhang.mapper.SettleMapper;
import com.miyuan.jizhang.mapper.UserInfoMapper;
import com.miyuan.jizhang.request.BaseRequest;
import com.miyuan.jizhang.request.ListSettleHistoryRequestVo;
import com.miyuan.jizhang.response.CountAmountToBeSettledResponseVo;
import com.miyuan.jizhang.response.GetUserToUserOutMoneyResponseVo;
import com.miyuan.jizhang.response.ListSettleHistoryResponseVo;
import com.miyuan.jizhang.response.ListSettleResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.GroupService;
import com.miyuan.jizhang.service.SettleService;
import com.miyuan.jizhang.utils.BeanUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SettleServiceImpl extends ServiceImpl<SettleMapper, SettlePo> implements SettleService {

	@Autowired
	private SettleMapper settleMapper;

	@Autowired
	private SettleHistoryMapper settleHistoryMapper;

	@Autowired
	private SettleHistoryDetailMapper settleHistoryDetailMapper;

	@Autowired
	private GroupService groupService;

	@Autowired
	private MoneyMapper moneyMapper;
	
	@Autowired
	private UserInfoMapper userInfoMapper;

	@Override
	public PageResult<ListSettleResponseVo> listSettle(BaseRequest vo, LoginUserInfoResponseVo user) {
		UserGroupPo userGroupPo = groupService.findGroupByUserId(user.getId());
		if (Objects.isNull(userGroupPo)) {
			throw new SettleException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		Page<SettlePo> page = new Page<SettlePo>(vo.getPageNumber(), vo.getPageSize());
		Wrapper<SettlePo> wrapper = new EntityWrapper<SettlePo>();
		wrapper.eq("jz_group_id", userGroupPo.getGroupId());
		wrapper.orderBy("jz_create_time", false);
		List<SettlePo> settlePos = settleMapper.selectPage(page, wrapper);
		List<ListSettleResponseVo> data = BeanUtils.convert(settlePos, ListSettleResponseVo.class);
		PageResult<ListSettleResponseVo> pageResult = new PageResult<ListSettleResponseVo>(data, vo.getPageNumber(),
				vo.getPageSize(), page.getTotal());
		return pageResult;
	}

	@Override
	public PageResult<ListSettleHistoryResponseVo> listSettleHistory(ListSettleHistoryRequestVo vo,
			LoginUserInfoResponseVo user) {
		List<ListSettleHistoryResponseVo> data = settleHistoryMapper.pageSettleHistory(vo.getSettleId(),
				vo.getPageNumber(), vo.getPageSize());
		Integer countSettleHistory = settleHistoryMapper.countSettleHistory(vo.getSettleId(), vo.getPageNumber(),
				vo.getPageSize());
		PageResult<ListSettleHistoryResponseVo> pageResult = new PageResult<ListSettleHistoryResponseVo>(data,
				vo.getPageNumber(), vo.getPageSize(), countSettleHistory);
		log.info("查询结算信息列表返回值：[{}]", pageResult.toString());
		return pageResult;
	}

	@Override
	public CountAmountToBeSettledResponseVo countAmountToBeSettled(LoginUserInfoResponseVo cUser) {
		List<UserGroupPo> userGroupPos = groupService.findMembersByuserId(cUser.getId());
		List<Integer> userGroupIds = null;
		CountAmountToBeSettledResponseVo vo = new CountAmountToBeSettledResponseVo();
		List<String> values = null;
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			userGroupIds = new ArrayList<Integer>(userGroupPos.size());
			values = new ArrayList<String>(userGroupPos.size());
			Iterator<UserGroupPo> iterator = userGroupPos.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				userGroupIds.add(userGroupPo.getId());
				// 查询每人实际消费金额
				BigDecimal countMoney = moneyMapper.countMoneyByUserGroupId(userGroupPo.getId());
				values.add(userGroupPo.getRealName() + "共消费" + (Objects.isNull(countMoney) ? 0 : countMoney) + "元整;");
			}
			vo.setValues(values);
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new SettleException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		EntityWrapper<MoneyPo> wrapper = new EntityWrapper<MoneyPo>();
		wrapper.in("jz_user_group_id", userGroupIds);
		wrapper.eq("jz_del", 0);
		wrapper.eq("jz_is_settle", 0);
		List<MoneyPo> moneyPos = moneyMapper.selectList(wrapper);
		if (CollectionUtils.isEmpty(moneyPos)) {
			vo.setTotal(BigDecimal.valueOf(0));
			vo.setAverage(BigDecimal.valueOf(0));
		} else {
			BigDecimal total = new BigDecimal(0);
			Iterator<MoneyPo> iterator = moneyPos.iterator();
			while (iterator.hasNext()) {
				MoneyPo moneyPo = (MoneyPo) iterator.next();
				total = total.add(moneyPo.getMoney());
			}
			vo.setTotal(total);
			BigDecimal average = total.divide(BigDecimal.valueOf(userGroupIds.size()), 2, BigDecimal.ROUND_HALF_UP);
			vo.setAverage(average);
		}
		return vo;
	}

	@Override
	public List<GetUserToUserOutMoneyResponseVo> listSettleHistoryDetail(ListSettleHistoryRequestVo vo) {
		List<GetUserToUserOutMoneyResponseVo> result = null;
		Wrapper<SettleHistoryDetailPo> wrapper = new EntityWrapper<SettleHistoryDetailPo>();
		wrapper.eq("jz_settle_id", vo.getSettleId());
		wrapper.eq("jz_del", 0);
		List<SettleHistoryDetailPo> settleHistoryDetailPos = settleHistoryDetailMapper.selectList(wrapper);
		if (CollectionUtils.isNotEmpty(settleHistoryDetailPos)) {
			result = new ArrayList<>(settleHistoryDetailPos.size());
			GetUserToUserOutMoneyResponseVo responseVo = null;
			Iterator<SettleHistoryDetailPo> iterator = settleHistoryDetailPos.iterator();
			while (iterator.hasNext()) {
				SettleHistoryDetailPo po = (SettleHistoryDetailPo) iterator.next();
				responseVo = new GetUserToUserOutMoneyResponseVo();
				responseVo.setMoney(po.getMoney());
				responseVo.setSendUserId(po.getSendUserId());
				responseVo.setReceiveUserId(po.getReceiveUserId());
				UserInfoPo sendUser = userInfoMapper.selectById(po.getSendUserId());
				responseVo.setSendUserName(sendUser.getRealName());
				UserInfoPo receiveUser = userInfoMapper.selectById(po.getReceiveUserId());
				responseVo.setReceiveUserName(receiveUser.getRealName());
				result.add(responseVo);
			}
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void settle(Integer userId) {
		// 登录人所在群组的所有组员
		List<UserGroupPo> userGroupPos = groupService.findMembersByuserId(userId);
		List<Integer> userGroupIds = null;
		Map<Integer, BigDecimal> map = null;
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			userGroupIds = new ArrayList<Integer>(userGroupPos.size());
			map = new HashMap<Integer, BigDecimal>(userGroupPos.size());
			Iterator<UserGroupPo> iterator = userGroupPos.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				userGroupIds.add(userGroupPo.getId());
				// 查询每人实际消费金额
				BigDecimal countMoney = moneyMapper.countMoneyByUserGroupId(userGroupPo.getId());
				map.put(userGroupPo.getId(), Objects.isNull(countMoney) ? BigDecimal.valueOf(0) : countMoney);
			}
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new SettleException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		Wrapper<MoneyPo> wrapper01 = new EntityWrapper<MoneyPo>();
		wrapper01.in("jz_user_group_id", userGroupIds);
		wrapper01.eq("jz_is_settle", 0);
		wrapper01.eq("jz_del", 0);
		List<MoneyPo> selectList = moneyMapper.selectList(wrapper01);
		if (CollectionUtils.isEmpty(selectList)) {
			throw new SettleException(ExceptionCodeEnum.NOT_FIND_HISTORY_OF_PAY_ERROR);
		}
		// 查询所有组员的所有本期消费记录
		EntityWrapper<MoneyPo> wrapper = new EntityWrapper<MoneyPo>();
		wrapper.in("jz_user_group_id", userGroupIds);
		wrapper.eq("jz_del", 0);
		wrapper.eq("jz_is_settle", 0);
		List<MoneyPo> moneyPos = moneyMapper.selectList(wrapper);
		BigDecimal total = new BigDecimal(0);
		List<Integer> moneyPoIds = null;
		if (CollectionUtils.isNotEmpty(moneyPos)) {
			moneyPoIds = new ArrayList<Integer>(moneyPos.size());
			Iterator<MoneyPo> iterator = moneyPos.iterator();
			while (iterator.hasNext()) {
				MoneyPo moneyPo = (MoneyPo) iterator.next();
				total = total.add(moneyPo.getMoney());
				moneyPoIds.add(moneyPo.getId());
			}
		}

		// 插入结算主表信息
		SettlePo settlePo = new SettlePo();
		settlePo.setSettleTotalMoney(total);
		settlePo.setGroupId(userGroupPos.get(0).getGroupId());
		settleMapper.insert(settlePo);

		// 插入结算详情表信息
		if (map != null && !map.isEmpty() && map.size() > 0) {
			SettleHistoryPo settleHistoryPo = null;
			Set<Integer> keySet = map.keySet();
			for (Integer userGroupId : keySet) {
				settleHistoryPo = new SettleHistoryPo();
				settleHistoryPo.setSettleId(settlePo.getId());
				settleHistoryPo.setUserGroupId(userGroupId);
				settleHistoryPo.setSettleMoney(map.get(userGroupId));
				settleHistoryMapper.insert(settleHistoryPo);
			}
		}

		// 插入结算详情之明细表信息
		List<GetUserToUserOutMoneyResponseVo> settle_function = settle_function(userGroupPos, userGroupIds, moneyPos);
		List<SettleHistoryDetailPo> settleHistoryDetailPos = null;
		if (CollectionUtils.isNotEmpty(settle_function)) {
			settleHistoryDetailPos = BeanUtils.convert(settle_function, SettleHistoryDetailPo.class);
		}
		if (CollectionUtils.isNotEmpty(settleHistoryDetailPos)) {
			Iterator<SettleHistoryDetailPo> iterator = settleHistoryDetailPos.iterator();
			while (iterator.hasNext()) {
				SettleHistoryDetailPo po = (SettleHistoryDetailPo) iterator.next();
				po.setSettleId(settlePo.getId());
				settleHistoryDetailMapper.insert(po);
			}
		}

		// 修改消费金额已结算
		if (CollectionUtils.isNotEmpty(moneyPoIds)) {
			moneyMapper.updateIsSettleById(moneyPoIds, 1);
		}
	}

	@Override
	public List<GetUserToUserOutMoneyResponseVo> userToUserOutMoney(Integer userId) {
		// 登录人所在群组的所有组员
		List<UserGroupPo> userGroupPos = groupService.findMembersByuserId(userId);
		List<Integer> userGroupIds = null;
		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			userGroupIds = new ArrayList<Integer>(userGroupPos.size());
			Iterator<UserGroupPo> iterator = userGroupPos.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				userGroupIds.add(userGroupPo.getId());
			}
		}
		if (CollectionUtils.isEmpty(userGroupIds)) {
			throw new SettleException(ExceptionCodeEnum.NOT_FIND_GROUP_MESSAGE_ERROR);
		}
		// 查询所有组员的所有本期消费记录
		EntityWrapper<MoneyPo> wrapper = new EntityWrapper<MoneyPo>();
		wrapper.in("jz_user_group_id", userGroupIds);
		wrapper.eq("jz_del", 0);
		wrapper.eq("jz_is_settle", 0);
		List<MoneyPo> moneyPos = moneyMapper.selectList(wrapper);
		List<GetUserToUserOutMoneyResponseVo> result = settle_function(userGroupPos, userGroupIds, moneyPos);
		return result;
	}

	/*
	 * 计算详情明细的方法
	 */
	private List<GetUserToUserOutMoneyResponseVo> settle_function(List<UserGroupPo> userGroupPos,
			List<Integer> userGroupIds, List<MoneyPo> moneyPos) {
		// 平均值
		BigDecimal average = BigDecimal.valueOf(0);
		if (CollectionUtils.isNotEmpty(moneyPos)) {
			BigDecimal total = new BigDecimal(0);
			Iterator<MoneyPo> iterator = moneyPos.iterator();
			while (iterator.hasNext()) {
				MoneyPo moneyPo = (MoneyPo) iterator.next();
				total = total.add(moneyPo.getMoney());
			}
			average = total.divide(BigDecimal.valueOf(userGroupIds.size()), 2, BigDecimal.ROUND_HALF_UP);
		}
		double ever = average.doubleValue();

		List<GetUserToUserOutMoneyResponseVo> result = new ArrayList<GetUserToUserOutMoneyResponseVo>();

		if (CollectionUtils.isNotEmpty(userGroupPos)) {
			// 谁需要掏钱，掏多少钱，用键值对存
			Map<UserGroupPo, Double> userOfOutMoney = new HashMap<UserGroupPo, Double>();
			List<Map<UserGroupPo, Double>> listByUserOfOutMoney = new ArrayList<Map<UserGroupPo, Double>>();

			// 谁需要获得，获得多少钱，用键值对存
			Map<UserGroupPo, Double> userOfInMoney = new HashMap<UserGroupPo, Double>();
			List<Map<UserGroupPo, Double>> listByUserOfInMoney = new ArrayList<Map<UserGroupPo, Double>>();

			Iterator<UserGroupPo> iterator = userGroupPos.iterator();
			while (iterator.hasNext()) {
				UserGroupPo userGroupPo = (UserGroupPo) iterator.next();
				// 查询每人实际消费金额
				BigDecimal countMoney = moneyMapper.countMoneyByUserGroupId(userGroupPo.getId());
				double allmoney = !Objects.isNull(countMoney) ? countMoney.doubleValue() : 0;

				// 每人需要花费金额 - 当人实际花费金额
				double one = allmoney - ever;
				if (one > 0) {
					// 这说明当人需要另获得 (one的绝对值 )元钱
					userOfInMoney.put(userGroupPo, Math.abs(one));
				} else if (one < 0) {
					// 这说明当人需要再掏出 (one的绝对值 )元钱
					userOfOutMoney.put(userGroupPo, Math.abs(one));
				} else {
					// 这说明当人正好花费当月应当花费金额
				}
			}

			listByUserOfInMoney.add(userOfInMoney);
			listByUserOfOutMoney.add(userOfOutMoney);

			do {
				// listIterator()，这个方法可以实现边遍历边修改list集合
				ListIterator<Map<UserGroupPo, Double>> it01 = listByUserOfInMoney.listIterator();
				loop: while (it01.hasNext()) {
					Map<UserGroupPo, Double> inputUserMoney = (Map<UserGroupPo, Double>) it01.next();
					Set<UserGroupPo> keyInputSet = inputUserMoney.keySet();
					for (UserGroupPo keyInput : keyInputSet) {
						Double valueInput = inputUserMoney.get(keyInput);
						ListIterator<Map<UserGroupPo, Double>> it02 = listByUserOfOutMoney.listIterator();
						while (it02.hasNext()) {
							Map<UserGroupPo, Double> outPutUserMoney = (Map<UserGroupPo, Double>) it02.next();
							Set<UserGroupPo> keyOutputSet = outPutUserMoney.keySet();
							for (UserGroupPo keyOutput : keyOutputSet) {
								Double valueOutput = outPutUserMoney.get(keyOutput);
								// 当人（keyOutput）要掏的金额 - 当人（keyInput）要获得的金额
								Double d = valueOutput - valueInput;
								GetUserToUserOutMoneyResponseVo vo = new GetUserToUserOutMoneyResponseVo();
								if (d > 0) {
									// 这里说明当人（keyInput） 的金额都获取到了，但当人（keyOutput）还要再掏出 (d的绝对值) 元钱
									userOfOutMoney.put(keyOutput, Math.abs(d));
									userOfInMoney.remove(keyInput);
									it01.set(inputUserMoney);
									it02.set(userOfOutMoney);

									vo.setMoney(BigDecimal.valueOf(valueInput).setScale(2, BigDecimal.ROUND_HALF_UP));
								} else if (d < 0) {
									// 这里说明 当人（keyOutput）
									// 的金额都掏出了，但当人（keyInput）还要再获得 (d的绝对值) 元钱
									userOfInMoney.put(keyInput, Math.abs(d));
									it01.set(userOfInMoney);
									userOfOutMoney.remove(keyOutput);
									it02.set(userOfOutMoney);

									vo.setMoney(BigDecimal.valueOf(valueOutput).setScale(2, BigDecimal.ROUND_HALF_UP));
								} else {
									// 这里说明 当人（keyOutput）
									// 的金额都掏出了，并且当人（keyInput）的金额也正好获取到了
									userOfInMoney.remove(keyInput);
									userOfOutMoney.remove(keyOutput);
									it01.set(inputUserMoney);
									it02.set(userOfOutMoney);

									vo.setMoney(BigDecimal.valueOf(valueInput).setScale(2, BigDecimal.ROUND_HALF_UP));
								}
								vo.setSendUserId(keyOutput.getUserId());
								vo.setSendUserName(keyOutput.getRealName());
								vo.setReceiveUserId(keyInput.getUserId());
								vo.setReceiveUserName(keyInput.getRealName());
								result.add(vo);
								break loop;
							}
						}
					}
				}
				if (listByUserOfInMoney.size() == 1) {
					Map<UserGroupPo, Double> inre = listByUserOfInMoney.get(0);
					if (inre.isEmpty()) {
						listByUserOfInMoney.clear();
					}
				}
				if (listByUserOfOutMoney.size() == 1) {
					Map<UserGroupPo, Double> outre = listByUserOfOutMoney.get(0);
					if (outre.isEmpty()) {
						listByUserOfOutMoney.clear();
					}
				}
			} while (CollectionUtils.isNotEmpty(listByUserOfInMoney)
					&& CollectionUtils.isNotEmpty(listByUserOfOutMoney));
		}
		return result;
	}

}
