package com.miyuan.jizhang.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.UserInfoPo;

/**
 * 操作用户信息表
 * 
 * @author zhanglongfei
 *
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfoPo> {

	/**
	 * 根据用户ID修改is_del字段为1，表示逻辑删除
	 * 
	 * @param id
	 * @return
	 */
	Integer updateUserInfoStatusIsDel(@Param(value = "ids") List<Integer> ids);

}
