package com.miyuan.jizhang.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.MoneyPo;
import com.miyuan.jizhang.response.CountMoneyLineChartResponseVo;

/**
 * 金额记录表
 * 
 * @author zhanglongfei
 *
 */
@Mapper
public interface MoneyMapper extends BaseMapper<MoneyPo> {

	/**
	 * 根据ID批量修改结算状态
	 * 
	 * @param ids
	 * @param isSettle
	 * @return
	 */
	public Integer updateIsSettleById(@Param(value = "ids") List<Integer> ids,
			@Param(value = "isSettle") Integer isSettle);

	/**
	 * 根据记录ID修改is_del字段为1，表示逻辑删除
	 * 
	 * @param id
	 * @return
	 */
	Integer updateMoneyStatusIsDel(@Param(value = "ids") List<Integer> ids);

	/**
	 * 统计本期消费金额折线图数据
	 * 
	 * @param userGroupIds
	 * @return
	 */
	List<CountMoneyLineChartResponseVo> countMoneyLineChart(@Param(value = "userGroupIds") List<Integer> userGroupIds);
	
	/**
	 * 统计本期每个组员消费金额柱状图数据
	 * 
	 * @param userGroupIds
	 * @return
	 */
	List<CountMoneyLineChartResponseVo> countMoneyBarChart(@Param(value = "userGroupIds") List<Integer> userGroupIds);
	
	/**
	 * 统计本期每个组员消费金额饼状图数据
	 * 
	 * @param userGroupIds
	 * @return
	 */
	List<CountMoneyLineChartResponseVo> countMoneyPieChart(@Param(value = "userGroupIds") List<Integer> userGroupIds);
	
	/**
	 * 根据用户群组关联表id统计金额
	 * 
	 * @param userGroupId
	 * @return
	 */
	BigDecimal countMoneyByUserGroupId(@Param(value = "userGroupId") Integer userGroupId);

}
