package com.miyuan.jizhang.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.SettleHistoryDetailPo;

/**
 * 操作结算详情之明细表
 * 
 * @author zhanglongfei
 *
 */
@Mapper
public interface SettleHistoryDetailMapper extends BaseMapper<SettleHistoryDetailPo> {

}
