package com.miyuan.jizhang.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.GroupPo;
import com.miyuan.jizhang.request.ListGroupRequestVo;
import com.miyuan.jizhang.response.ListGroupResponseVo;

/**
 * 操作群组表
 * 
 * @author zhanglongfei
 *
 */
@Mapper
public interface GroupMapper extends BaseMapper<GroupPo> {

	/**
	 * 根据群ID修改is_del字段为1，表示逻辑删除
	 * 
	 * @param ids
	 * @return
	 */
	public Integer updateGroupStatusIsDel(@Param(value = "ids") List<Integer> ids);
	
	/**
	 * 分页查询记录
	 * 
	 * @param vo
	 * @return
	 */
	List<ListGroupResponseVo> list(ListGroupRequestVo vo, @Param(value = "curUserId") Integer curUserId);

	/**
	 * 查询总条数
	 * 
	 * @param vo
	 * @return
	 */
	Integer count(@Param(value = "vo") ListGroupRequestVo vo, @Param(value = "curUserId") Integer curUserId);
	
}
