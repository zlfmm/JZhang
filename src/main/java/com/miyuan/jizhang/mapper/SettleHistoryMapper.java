package com.miyuan.jizhang.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.SettleHistoryPo;
import com.miyuan.jizhang.response.ListSettleHistoryResponseVo;

/**
 * 操作结算历史表
 * 
 * @author zhanglongfei
 *
 */
@Mapper
public interface SettleHistoryMapper extends BaseMapper<SettleHistoryPo> {

	/**
	 * 分页查询结算详情信息列表
	 * 
	 * @param settleId
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	List<ListSettleHistoryResponseVo> pageSettleHistory(@Param(value = "settleId") Integer settleId,
			@Param(value = "pageNumber") Integer pageNumber, @Param(value = "pageSize") Integer pageSize);

	/**
	 * 统计结算详情信息列表
	 * 
	 * @param settleId
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	Integer countSettleHistory(@Param(value = "settleId") Integer settleId,
			@Param(value = "pageNumber") Integer pageNumber, @Param(value = "pageSize") Integer pageSize);

}
