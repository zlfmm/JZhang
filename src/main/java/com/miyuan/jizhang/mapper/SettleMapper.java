package com.miyuan.jizhang.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.SettlePo;

/**
 * 操作结算表
 * @author zhanglongfei
 *
 */
@Mapper
public interface SettleMapper extends BaseMapper<SettlePo> {

}
