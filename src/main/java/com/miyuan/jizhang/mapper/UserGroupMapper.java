package com.miyuan.jizhang.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.miyuan.jizhang.entity.UserGroupPo;

/**
 * 用户群组关联表
 * 
 * @author zhanglongfei
 *
 */
@Mapper
public interface UserGroupMapper extends BaseMapper<UserGroupPo> {

    void updateGroupNameByGroupId(@Param(value = "groupId") Integer groupId, @Param(value = "groupName") String groupName);
}
