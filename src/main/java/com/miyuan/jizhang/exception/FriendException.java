package com.miyuan.jizhang.exception;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

/**
 * 好友异常类
 * @author zhanglongfei
 *
 */
@SuppressWarnings("serial")
public class FriendException extends BaseException{

	public FriendException(ExceptionCodeEnum codeEnum) {
		super(codeEnum);
	}

}
