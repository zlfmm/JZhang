package com.miyuan.jizhang.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.alibaba.fastjson.JSON;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.utils.MiscUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 全局异常处理: 使用 @RestControllerAdvice + @ExceptionHandler 注解方式实现全局异常处理
 * 
 * @author zhanglongfei
 *
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	
	/**
     * 输入参数校验异常
     */
    @SuppressWarnings("rawtypes")
	@ExceptionHandler(value = MethodArgumentNotValidException.class)
    public BaseResult NotValidExceptionHandler(HttpServletRequest req, MethodArgumentNotValidException e) throws Exception {
        BindingResult bindingResult = e.getBindingResult();
        BaseResult res = MiscUtils.getValidateError(bindingResult);
        return res;
    }
    

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler({ Exception.class }) // 申明捕获哪个异常类
	public BaseResult globalExceptionHandler(Exception e) {
		return new BaseResult(e.getMessage());
	}

	/**
	 * 自定义异常处理
	 * @param e
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@ExceptionHandler({ BaseException.class })
	public BaseResult BaseExceptionHandler(ExceptionCodeEnum e) {
		return new BaseResult(e);
	}

}
