package com.miyuan.jizhang.exception;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

/**
 * 自定义群组异常类
 * @author zhanglongfei
 *
 */
@SuppressWarnings("serial")
public class GroupException extends BaseException{

	public GroupException(ExceptionCodeEnum codeEnum) {
		super(codeEnum);
	}

}
