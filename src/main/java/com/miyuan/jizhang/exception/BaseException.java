package com.miyuan.jizhang.exception;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

@SuppressWarnings("serial")
public class BaseException extends RuntimeException {
	
	private Integer code;
	
	// 给子类用的方法
    public BaseException(ExceptionCodeEnum codeEnum) {
        this(codeEnum.getMsg(), codeEnum.getCode());
    }

    private BaseException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
