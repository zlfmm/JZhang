package com.miyuan.jizhang.exception;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

/**
 * 结算异常类
 * @author zhanglongfei
 *
 */
@SuppressWarnings("serial")
public class SettleException extends BaseException{

	public SettleException(ExceptionCodeEnum codeEnum) {
		super(codeEnum);
	}

}
