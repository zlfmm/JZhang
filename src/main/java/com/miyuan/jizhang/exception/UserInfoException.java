package com.miyuan.jizhang.exception;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

/**
 * 自定义用户异常
 * 
 * @author zhanglongfei
 *
 */
@SuppressWarnings("serial")
public class UserInfoException extends BaseException {// 继承 业务异常类

	public UserInfoException(ExceptionCodeEnum codeEnum) {
		super(codeEnum);
	}

}
