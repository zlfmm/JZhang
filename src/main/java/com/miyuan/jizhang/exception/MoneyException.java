package com.miyuan.jizhang.exception;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

/**
 * 消费记录异常类
 * @author zhanglongfei
 *
 */
@SuppressWarnings("serial")
public class MoneyException extends BaseException{

	public MoneyException(ExceptionCodeEnum codeEnum) {
		super(codeEnum);
	}

}
