package com.miyuan.jizhang.enums;

public enum ExceptionCodeEnum {

	/** 成功 */
	SUCCESS(200, "成功"),

	/** 发生异常 */
	EXCEPTION(401, "发生异常"),

	/** 系统错误 */
	SYS_ERROR(402, "系统错误"),
	
	/** 服务器无法响应 */
	SUTR_ERROR(404, "服务器无法响应"),

	/** 网络错误 */
	NETWORK_ERROR(9999,"网络错误，待会重试"),
	
	/** 操作失败 */
	FAIL(10001, "操作失败"),
	
	/** 用户未登录 */
	NOT_LOGGED_IN_YET(10002,"用户未登录"),
	
	/** 操作失败 */
	REQUEST_PARAM_ERROR(10003, "请求参数异常"),
	
	/** 邮箱已存在 */
	ADDRESS_ALREADY_INUSE(10004, "该邮箱已注册"),
	
	/** 用户名已存在 */
	USER_NAME_ALREADY_INUSE(10005, "该用户名已存在"),
	
	/** 用户名不正确或不存在 */
	USER_NAME_INPUT_ERROR(10006, "用户名不正确或不存在"),
	
	/** 密码错误 */
	PASS_WORD_INPUT_ERROR(10007, "密码错误"),
	
	/** 未查询到分组信息 */
	NOT_FIND_GROUP_MESSAGE_ERROR(10008, "未查询到分组信息"),
	
	/** 您只能删除自己的记录 */
	DELETE_ONLY_YOURSELF_PAY_ERROR(10009, "您只能删除自己的记录"),
	
	/** 未查询到您的记录 */
	NOT_FIND_YOUR_PAY_HISTORY_ERROR(10010, "未查询到您的记录"),
	
	/** 输入的描述过长 */
	LONG_DETAIL_INSERT_ERROR(10011, "输入的描述过长"),
	
	/** 已存在分组中 */
	ALREADY_IN_GROUP_ERROR(10012, "你已经存在一个分组中"),
	
	/** 此群组名已被注册,请换一个 */
	CHECK_GROUP_IS_IN_USE_ERROR(10013, "此群组名已被注册,请换一个"),
	
	/** 一个群组最多八个成员 */
	GROUP_MAST_MEMBER_ERROR(10014, "一个群组最多八个成员"),
	
	/** 被邀请人已存在分组中 */
	INVITATE_USER_ALREADY_IN_GROUP_ERROR(10015, "被邀请人已存在分组中"),
	
	/** 您无权解散,请通知创建者解散 */
	TALK_TO_CREATE_USER_DISSOLUTE_ERROR(10016, "您无权解散，请通知创建者解散"),
	
	/** 本期不存在消费记录 */
	NOT_FIND_HISTORY_OF_PAY_ERROR(10016, "本期不存在消费记录"),
	;

	private ExceptionCodeEnum(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public String msg() {
		return msg;
	}

	private Integer code;
	private String msg;

}
