package com.miyuan.jizhang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import io.swagger.annotations.Api;

@Api(tags = "页面跳转控制操作", value = "页面跳转控制操作")
@Controller
public class PageController {
	
	/** 跳转到登录页面 */
	@GetMapping(value = {"/","/tologin"})
	public String login() {
		return "login";
	}
	
	/** 跳转到注册页面 */
	@GetMapping("/toregister")
	public String register() {
		return "register";
	}
	
	/** 跳转到首页页面 */
	@GetMapping("/toindex")
    public String toindex() {
        return "index";
    }
	
	/** 跳转到记一笔页面 */
	@GetMapping("/tolist")
    public String tolist() {
        return "list";
    }
	
	/** 跳转到待结算页面 */
	@GetMapping("/tosettlement")
    public String tosettlement() {
        return "settlement";
    }
	
	/** 跳转到结算历史页面 */
	@GetMapping("/tohistory")
    public String tohistory() {
        return "listHistory";
    }
	
}
