package com.miyuan.jizhang.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miyuan.jizhang.entity.UserGroupPo;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.FriendException;
import com.miyuan.jizhang.exception.GroupException;
import com.miyuan.jizhang.request.DeleteGroupRequestVo;
import com.miyuan.jizhang.request.InsertGroupRequestVo;
import com.miyuan.jizhang.request.ListGroupRequestVo;
import com.miyuan.jizhang.request.UpdateGroupRequestVo;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.response.ListGroupResponseVo;
import com.miyuan.jizhang.response.ListUserInfoResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.GroupService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "群操作", value = "群操作")
@RestController
@RequestMapping(value = "/group")
@Slf4j
public class GroupController extends BaseController {

	@Autowired
	private GroupService groupService;
	
	@ApiOperation(value = "新增群组信息", notes = "新增群组信息，即注册", response = Boolean.class)
	@PostMapping(value = "/add")
	public BaseResult<Boolean> addGroup(Model model, @Valid @RequestBody InsertGroupRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			boolean data = groupService.addGroup(vo, userInfoResponseVo);
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (GroupException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("新增群组信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "查询群组信息列表", notes = "查询群组信息列表，可条件查询", response = ListUserInfoResponseVo.class)
	@PostMapping(value = "/list")
	public BaseResult<PageResult<ListGroupResponseVo>> listGroup(Model model, @Valid @RequestBody ListGroupRequestVo vo) {
		BaseResult<PageResult<ListGroupResponseVo>> result = new BaseResult<PageResult<ListGroupResponseVo>>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			PageResult<ListGroupResponseVo> pageResult = groupService.listUserInfo(vo, userInfoResponseVo);
			result = new BaseResult<PageResult<ListGroupResponseVo>>(pageResult, ExceptionCodeEnum.SUCCESS);
		} catch (GroupException e) {
			result = new BaseResult<PageResult<ListGroupResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("查询群组信息列表异常!", e);
			result = new BaseResult<PageResult<ListGroupResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "根据用户ID修改群组信息", notes = "根据用户ID修改群组信息，用户ID必填", response = Boolean.class)
	@PostMapping(value = "/updatebyid")
	public BaseResult<Boolean> listGroupById(@Valid @RequestBody UpdateGroupRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			boolean data = groupService.updateUserInfoById(vo);
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (GroupException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据用户ID修改群组信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "根据用户ID删除群组信息", notes = "根据用户ID删除群组信息，用户ID必填", response = Boolean.class)
	@PostMapping(value = "/deletebyid")
	public BaseResult<Boolean> deleteUserInfoById(@Valid @RequestBody DeleteGroupRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			boolean data = groupService.deleteUserInfoById(vo);
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (GroupException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据用户ID删除群组信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "根据当前用户所属分组查询分组成员接口", notes = "根据当前用户所属分组查询分组成员接口", response = UserGroupPo.class)
	@PostMapping(value = "/findMembers")
	public BaseResult<List<UserGroupPo>> findMembers(Model model) {
		BaseResult<List<UserGroupPo>> result = new BaseResult<List<UserGroupPo>>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			List<UserGroupPo> userGroupPos = groupService.findMembersByuserId(userInfoResponseVo.getId());
			result = new BaseResult<List<UserGroupPo>>(userGroupPos, ExceptionCodeEnum.SUCCESS);
		} catch (FriendException e) {
			result = new BaseResult<List<UserGroupPo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据当前用户所属分组查询分组成员异常!", e);
			result = new BaseResult<List<UserGroupPo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "邀请入群组接口", notes = "邀请入群组接口", response = Boolean.class)
	@PostMapping(value = "/invitateUser")
	public BaseResult<Boolean> invitateUser(Model model, @RequestParam(value = "uId", required = true) Integer uId) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			boolean data = groupService.invitateUser(userInfoResponseVo.getId(), uId);
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (GroupException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("邀请入群组接口异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "解散群组接口", notes = "解散入群组接口", response = Boolean.class)
	@PostMapping(value = "/dissoluteGroup")
	public BaseResult<Boolean> dissoluteGroup(Model model) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			boolean data = groupService.dissoluteGroup(userInfoResponseVo.getId());
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (GroupException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("解散群组接口异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
}
