package com.miyuan.jizhang.controller;

import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.miyuan.jizhang.constant.JizhangConstant;
import com.miyuan.jizhang.listener.SessionListener;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;

import io.swagger.annotations.Api;

@Api(tags = "基础controller类", value = "基础controller类")
@RestController
@SessionAttributes(value = {JizhangConstant.CUR_USER})
public class BaseController {
	
	public LoginUserInfoResponseVo getCurUser(Model model){
		if(!model.containsAttribute(JizhangConstant.CUR_USER)){
			return null;
		}
		Map<String,Object> modelMap = model.asMap();
		LoginUserInfoResponseVo curUser = (LoginUserInfoResponseVo)modelMap.get(JizhangConstant.CUR_USER);
		if(null == curUser || curUser.getId() == null){
			return null;
		}
		return curUser;
	}
	
	/**
     * 通过用户ID来强行把已经在线的用户踢出，当发现账号已经被人登陆了，就将这个已经登陆上的人的Session从SessionListener.java中的HashMap里给拿到，并且移除在此HashMap中的记录
     *
     * @param uid
     */
    public static void forceLogoutUser(Integer uid) {
        // 删除单一登录中记录的变量
        if (SessionListener.sessionMap.get(uid) != null) {
            HttpSession hs = (HttpSession) SessionListener.sessionMap.get(uid);
            SessionListener.sessionMap.remove(uid);
            Enumeration e = hs.getAttributeNames();
            while (e.hasMoreElements()) {
                String sessionName = (String) e.nextElement();
                // 清空session
                hs.removeAttribute(sessionName);
            }
            // hs.invalidate();
        }
    }
	
}
