package com.miyuan.jizhang.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.UserInfoException;
import com.miyuan.jizhang.request.DeleteUserInfoRequestVo;
import com.miyuan.jizhang.request.InsertUserInfoRequestVo;
import com.miyuan.jizhang.request.ListUserInfoRequestVo;
import com.miyuan.jizhang.request.UpdateUserInfoRequestVo;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.response.ListUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.UserInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "用户操作", value = "用户操作")
@RestController
@RequestMapping(value = "/userinfo")
@Slf4j
public class UserInfoController {
	
	@Autowired
	private UserInfoService userInfoService;
	
	@ApiOperation(value = "新增用户信息", notes = "新增用户信息，即注册", response = Boolean.class)
	@PostMapping(value = "/add")
	public BaseResult<Boolean> addUserInfo(@Valid @RequestBody InsertUserInfoRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			boolean data = userInfoService.addUserInfo(vo);
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (UserInfoException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("新增用户信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "查询用户信息列表", notes = "查询用户信息列表，可条件查询", response = ListUserInfoResponseVo.class)
	@PostMapping(value = "/list")
	public BaseResult<PageResult<ListUserInfoResponseVo>> listUserInfo(@Valid @RequestBody ListUserInfoRequestVo vo) {
		BaseResult<PageResult<ListUserInfoResponseVo>> result = new BaseResult<PageResult<ListUserInfoResponseVo>>();
		try {
			PageResult<ListUserInfoResponseVo> pageResult = userInfoService.listUserInfo(vo);
			result = new BaseResult<PageResult<ListUserInfoResponseVo>>(pageResult, ExceptionCodeEnum.SUCCESS);
		} catch (UserInfoException e) {
			result = new BaseResult<PageResult<ListUserInfoResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("查询用户信息列表异常!", e);
			result = new BaseResult<PageResult<ListUserInfoResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "根据用户ID修改用户信息", notes = "根据用户ID修改用户信息，用户ID必填", response = Boolean.class)
	@PostMapping(value = "/updatebyid")
	public BaseResult<Boolean> listUserInfoById(@Valid @RequestBody UpdateUserInfoRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			boolean success = userInfoService.updateUserInfoById(vo);
			result = new BaseResult<Boolean>(success, ExceptionCodeEnum.SUCCESS);
		} catch (UserInfoException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据用户ID修改用户信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "根据用户ID删除用户信息", notes = "根据用户ID删除用户信息，用户ID必填", response = Boolean.class)
	@PostMapping(value = "/deletebyid")
	public BaseResult<Boolean> deleteUserInfoById(@Valid @RequestBody DeleteUserInfoRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			boolean success = userInfoService.deleteUserInfoById(vo);
			result = new BaseResult<Boolean>(success, ExceptionCodeEnum.SUCCESS);
		} catch (UserInfoException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据用户ID删除用户信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "根据邮箱模糊查询用户信息列表", notes = "根据邮箱模糊查询用户信息列表", response = ListUserInfoResponseVo.class)
	@PostMapping(value = "/listUserInfoByEmail")
	public BaseResult<List<ListUserInfoResponseVo>> listUserInfoByEmail(@RequestParam("address") String address) {
		BaseResult<List<ListUserInfoResponseVo>> result = new BaseResult<List<ListUserInfoResponseVo>>();
		try {
			List<ListUserInfoResponseVo> datas = userInfoService.listUserInfoByEmail(address);
			result = new BaseResult<List<ListUserInfoResponseVo>>(datas, ExceptionCodeEnum.SUCCESS);
		} catch (UserInfoException e) {
			result = new BaseResult<List<ListUserInfoResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据邮箱模糊查询用户信息列表异常!", e);
			result = new BaseResult<List<ListUserInfoResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

}
