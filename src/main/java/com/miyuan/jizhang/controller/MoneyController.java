package com.miyuan.jizhang.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.MoneyException;
import com.miyuan.jizhang.request.DeleteMoneyByIdsRequestVo;
import com.miyuan.jizhang.request.InsertMoneyRequestVo;
import com.miyuan.jizhang.request.ListMoneyRequestVo;
import com.miyuan.jizhang.request.UpdateMoneyToSettleRequestVo;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.response.CountMoneyLineChartResponseVo;
import com.miyuan.jizhang.response.ListMoneyResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.MoneyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "消费记录", value = "消费记录")
@RestController
@RequestMapping(value = "/money")
@Slf4j
public class MoneyController extends BaseController{

	@Autowired
	private MoneyService moneyService;

	@ApiOperation(value = "新增消费记录", notes = "新增消费", response = Boolean.class)
	@PostMapping(value = "/add")
	public BaseResult<Boolean> addUserInfo(Model model, @Valid @RequestBody InsertMoneyRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			boolean success = moneyService.addMoney(vo, userInfoResponseVo);
			result = new BaseResult<Boolean>(success, ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("新增消费记录异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "删除消费记录", notes = "可批量删除消费记录", response = Boolean.class)
	@PostMapping(value = "/delete")
	public BaseResult<Boolean> deleteUserInfo(Model model, @Valid @RequestBody DeleteMoneyByIdsRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		LoginUserInfoResponseVo curUser = getCurUser(model);
		try {
			boolean data = moneyService.deleteMoneyByIds(curUser, vo);
			result = new BaseResult<Boolean>(data, ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("删除消费记录异常异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "条件获取消费记录", notes = "条件获取消费记录", response = ListMoneyResponseVo.class)
	@PostMapping(value = "/list")
	public BaseResult<PageResult<ListMoneyResponseVo>> listUserInfo(Model model, @Valid @RequestBody ListMoneyRequestVo vo) {
		BaseResult<PageResult<ListMoneyResponseVo>> result = new BaseResult<PageResult<ListMoneyResponseVo>>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			vo.setUserId(userInfoResponseVo.getId());
			PageResult<ListMoneyResponseVo> listMoney = moneyService.listMoney(vo);
			result = new BaseResult<PageResult<ListMoneyResponseVo>>(listMoney,
					ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<PageResult<ListMoneyResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("条件获取消费记录异常!", e);
			result = new BaseResult<PageResult<ListMoneyResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "根据ID结算消费信息", notes = "根据ID结算消费信息", response = Boolean.class)
	@PostMapping(value = "/updateByIds")
	public BaseResult<Boolean> updateByIds(@Valid @RequestBody UpdateMoneyToSettleRequestVo vo) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			boolean moneyByIds = moneyService.settleMoneyByIds(vo.getIds());
			result = new BaseResult<Boolean>(moneyByIds, ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<Boolean>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("根据ID结算消费信息异常!", e);
			result = new BaseResult<Boolean>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "统计本期消费金额折线图数据接口", notes = "统计本期消费金额折线图数据接口", response = CountMoneyLineChartResponseVo.class)
	@PostMapping(value = "/countMoneyLineChart")
	public BaseResult<List<CountMoneyLineChartResponseVo>> countMoneyLineChart(Model model) {
		BaseResult<List<CountMoneyLineChartResponseVo>> result = new BaseResult<List<CountMoneyLineChartResponseVo>>();
		try {
			LoginUserInfoResponseVo curUser = getCurUser(model);
			List<CountMoneyLineChartResponseVo> data = moneyService.countMoneyLineChart(curUser);
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(data, ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("统计本期消费金额折线图数据异常!", e);
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "统计本期每个组员消费金额柱状图数据接口", notes = "统计本期每个组员消费金额柱状图数据接", response = CountMoneyLineChartResponseVo.class)
	@PostMapping(value = "/countMoneyBarChart")
	public BaseResult<List<CountMoneyLineChartResponseVo>> countMoneyBarChart(Model model) {
		BaseResult<List<CountMoneyLineChartResponseVo>> result = new BaseResult<List<CountMoneyLineChartResponseVo>>();
		try {
			LoginUserInfoResponseVo curUser = getCurUser(model);
			List<CountMoneyLineChartResponseVo> data = moneyService.countMoneyBarChart(curUser);
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(data, ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("统计本期每个组员消费金额柱状图数据异常!", e);
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "统计本期每个组员消费金额饼状图数据接口", notes = "统计本期每个组员消费金额饼状图数据接", response = CountMoneyLineChartResponseVo.class)
	@PostMapping(value = "/countMoneyPieChart")
	public BaseResult<List<CountMoneyLineChartResponseVo>> countMoneyPieChart(Model model) {
		BaseResult<List<CountMoneyLineChartResponseVo>> result = new BaseResult<List<CountMoneyLineChartResponseVo>>();
		try {
			LoginUserInfoResponseVo curUser = getCurUser(model);
			List<CountMoneyLineChartResponseVo> data = moneyService.countMoneyBarChart(curUser);
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(data, ExceptionCodeEnum.SUCCESS);
		} catch (MoneyException e) {
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("统计本期每个组员消费金额饼状图数据异常!", e);
			result = new BaseResult<List<CountMoneyLineChartResponseVo>>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

}
