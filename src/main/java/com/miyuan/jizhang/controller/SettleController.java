package com.miyuan.jizhang.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.SettleException;
import com.miyuan.jizhang.request.BaseRequest;
import com.miyuan.jizhang.request.ListSettleHistoryRequestVo;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.response.CountAmountToBeSettledResponseVo;
import com.miyuan.jizhang.response.GetUserToUserOutMoneyResponseVo;
import com.miyuan.jizhang.response.ListSettleHistoryResponseVo;
import com.miyuan.jizhang.response.ListSettleResponseVo;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.response.PageResult;
import com.miyuan.jizhang.service.SettleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "结算记录", value = "结算记录")
@RestController
@RequestMapping(value = "/settle")
@Slf4j
public class SettleController extends BaseController {

	@Autowired
	private SettleService settleService;

	@ApiOperation(value = "分页查询结算主表记录接口", notes = "分页查询结算主表记录接口", response = ListSettleResponseVo.class)
	@PostMapping(value = "/listSettle")
	public BaseResult<PageResult<ListSettleResponseVo>> listSettle(Model model, @Valid @RequestBody BaseRequest vo) {
		BaseResult<PageResult<ListSettleResponseVo>> result = new BaseResult<PageResult<ListSettleResponseVo>>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			PageResult<ListSettleResponseVo> list = settleService.listSettle(vo, userInfoResponseVo);
			result = new BaseResult<PageResult<ListSettleResponseVo>>(list, ExceptionCodeEnum.SUCCESS);
		} catch (SettleException e) {
			result = new BaseResult<PageResult<ListSettleResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("分页查询结算主表记录异常!", e);
			result = new BaseResult<PageResult<ListSettleResponseVo>>(ExceptionCodeEnum.FAIL.getCode(),
					ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "分页查询结算详情记录接口", notes = "分页查询结算详情记录接口", response = ListSettleHistoryResponseVo.class)
	@PostMapping(value = "/listSettleHistory")
	public BaseResult<PageResult<ListSettleHistoryResponseVo>> listSettleHistory(Model model,
			@Valid @RequestBody ListSettleHistoryRequestVo vo) {
		BaseResult<PageResult<ListSettleHistoryResponseVo>> result = new BaseResult<PageResult<ListSettleHistoryResponseVo>>();
		try {
			LoginUserInfoResponseVo userInfoResponseVo = getCurUser(model);
			PageResult<ListSettleHistoryResponseVo> list = settleService.listSettleHistory(vo, userInfoResponseVo);
			result = new BaseResult<PageResult<ListSettleHistoryResponseVo>>(list, ExceptionCodeEnum.SUCCESS);
		} catch (SettleException e) {
			result = new BaseResult<PageResult<ListSettleHistoryResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("分页查询结算详情记录异常!", e);
			result = new BaseResult<PageResult<ListSettleHistoryResponseVo>>(ExceptionCodeEnum.FAIL.getCode(),
					ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "分页查询结算详情明细记录接口", notes = "分页查询结算详情明细记录接口", response = ListSettleHistoryResponseVo.class)
	@PostMapping(value = "/listSettleHistoryDetail")
	public BaseResult<List<GetUserToUserOutMoneyResponseVo>> listSettleHistoryDetail(
			@Valid @RequestBody ListSettleHistoryRequestVo vo) {
		BaseResult<List<GetUserToUserOutMoneyResponseVo>> result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>();
		try {
			List<GetUserToUserOutMoneyResponseVo> list = settleService.listSettleHistoryDetail(vo);
			result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>(list, ExceptionCodeEnum.SUCCESS);
		} catch (SettleException e) {
			result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("分页查询结算详情明细记录异常!", e);
			result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>(ExceptionCodeEnum.FAIL.getCode(),
					ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "待结算总金额", notes = "待结算总金额", response = CountAmountToBeSettledResponseVo.class)
	@PostMapping(value = "/countAmountToBeSettled")
	public BaseResult<CountAmountToBeSettledResponseVo> countAmountToBeSettled(Model model) {
		BaseResult<CountAmountToBeSettledResponseVo> result = new BaseResult<CountAmountToBeSettledResponseVo>();
		try {
			LoginUserInfoResponseVo cUser = getCurUser(model);
			CountAmountToBeSettledResponseVo countSettleHistory = settleService.countAmountToBeSettled(cUser);
			result = new BaseResult<CountAmountToBeSettledResponseVo>(countSettleHistory, ExceptionCodeEnum.SUCCESS);
		} catch (SettleException e) {
			result = new BaseResult<CountAmountToBeSettledResponseVo>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("结算总金额异常!", e);
			result = new BaseResult<CountAmountToBeSettledResponseVo>(ExceptionCodeEnum.FAIL.getCode(),
					ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@ApiOperation(value = "查询本期结算详情之明细表", notes = "查询本期结算详情之明细表", response = GetUserToUserOutMoneyResponseVo.class)
	@PostMapping(value = "/userToUserOutMoney")
	public BaseResult<List<GetUserToUserOutMoneyResponseVo>> userToUserOutMoney(Model model) {
		BaseResult<List<GetUserToUserOutMoneyResponseVo>> result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>();
		try {
			LoginUserInfoResponseVo cUser = getCurUser(model);
			List<GetUserToUserOutMoneyResponseVo> countSettleHistory = settleService.userToUserOutMoney(cUser.getId());
			result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>(countSettleHistory,
					ExceptionCodeEnum.SUCCESS);
		} catch (SettleException e) {
			result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("结算总金额异常!", e);
			result = new BaseResult<List<GetUserToUserOutMoneyResponseVo>>(ExceptionCodeEnum.FAIL.getCode(),
					ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "结算接口", notes = "结算接口")
	@PostMapping(value = "/settle")
	public BaseResult settle(Model model) {
		BaseResult result = new BaseResult();
		try {
			LoginUserInfoResponseVo cUser = getCurUser(model);
			settleService.settle(cUser.getId());
			result = new BaseResult(ExceptionCodeEnum.SUCCESS);
		} catch (SettleException e) {
			result = new BaseResult(e.getCode(), e.getMessage());
		} catch (Exception e) {
			log.error("结算接口异常!", e);
			result = new BaseResult(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}

}
