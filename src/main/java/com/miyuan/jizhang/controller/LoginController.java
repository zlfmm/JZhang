package com.miyuan.jizhang.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.miyuan.jizhang.constant.JizhangConstant;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.exception.UserInfoException;
import com.miyuan.jizhang.listener.SessionListener;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;
import com.miyuan.jizhang.service.UserInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "登录登出操作", value = "登录登出操作")
@RestController
@SessionAttributes(value = {JizhangConstant.CUR_USER})
@Slf4j
public class LoginController extends BaseController{

	@Autowired
	private UserInfoService userInfoService;

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "登录", notes = "登录", response = LoginUserInfoResponseVo.class)
	@GetMapping("/login")
	public BaseResult<LoginUserInfoResponseVo> login(HttpServletRequest request, HttpServletResponse response,
			Model model, @ApiParam(value = "用户名或邮箱") @RequestParam(value = "username", required = true) String username,
			@ApiParam(value = "密码") @RequestParam(value = "password", required = true) String password) {
		BaseResult<LoginUserInfoResponseVo> result = new BaseResult<LoginUserInfoResponseVo>();
		try {
			LoginUserInfoResponseVo data = userInfoService.login(username, password);
			HttpSession session = request.getSession(true);
			if (null == data) {
                result.setErrCode(ExceptionCodeEnum.USER_NAME_INPUT_ERROR.getCode());
                result.setErrMsg(ExceptionCodeEnum.USER_NAME_INPUT_ERROR.getMsg());
                return result;
            }
			if (null != SessionListener.sessionMap.get(data.getId())) {
                //该账号已经被登陆
                //将第一次登录用户的信息从map中移除
                forceLogoutUser(data.getId());
                //本次登录用户添加到map中
                SessionListener.sessionMap.put(data.getId(), session);
                model.addAttribute("curUser", data);
            } else {
                //以用户id为key键存入map中，以判断下一次登录的人
                SessionListener.sessionMap.put(data.getId(), session);
                model.addAttribute("curUser", data);
            }
			result = new BaseResult<LoginUserInfoResponseVo>(data, ExceptionCodeEnum.SUCCESS);
		} catch (UserInfoException e) {
			result.setErrCode(e.getCode());
			result.setErrMsg(e.getMessage());
		} catch (Exception e) {
			log.error("登录异常!", e);
			result = new BaseResult<LoginUserInfoResponseVo>(ExceptionCodeEnum.FAIL.getCode(), ExceptionCodeEnum.FAIL.getMsg());
		}
		return result;
	}
	
	@ApiOperation(value = "登出", notes = "登出", response = Boolean.class)
	@GetMapping("/logout")
	public BaseResult<Boolean> logout(HttpServletRequest request, HttpServletResponse response, Model model) {
		BaseResult<Boolean> result = new BaseResult<Boolean>();
		try {
			LoginUserInfoResponseVo curUser = getCurUser(model);
			if (null == curUser || null == SessionListener.sessionMap.get(curUser.getId())) {
				result.setErrCode(ExceptionCodeEnum.NOT_LOGGED_IN_YET.getCode());
                result.setErrMsg(ExceptionCodeEnum.NOT_LOGGED_IN_YET.getMsg());
                return result;
			}
			forceLogoutUser(curUser.getId());
			result = new BaseResult<Boolean>(ExceptionCodeEnum.SUCCESS);
		} catch (Exception e) {
			result = new BaseResult<Boolean>(ExceptionCodeEnum.EXCEPTION);
			e.printStackTrace();
		}
		return result;
	}
	

}
