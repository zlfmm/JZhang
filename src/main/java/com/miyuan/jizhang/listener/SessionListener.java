package com.miyuan.jizhang.listener;

import java.util.HashMap;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.miyuan.jizhang.response.LoginUserInfoResponseVo;

@WebListener
public class SessionListener implements HttpSessionListener {
	
	public static HashMap sessionMap = new HashMap();

    public void sessionCreated(HttpSessionEvent hse) {
        HttpSession session = hse.getSession();
    }

    public void sessionDestroyed(HttpSessionEvent hse) {
        HttpSession session = hse.getSession();
        this.DelSession(session);
    }

    public static synchronized void DelSession(HttpSession session) {
        if (session != null) {
            // 删除单一登录中记录的变量
            if (session.getAttribute("curUser") != null) {
            	LoginUserInfoResponseVo tu = (LoginUserInfoResponseVo) session.getAttribute("curUser");
                SessionListener.sessionMap.remove(tu.getId());
            }
        }
    }

}
