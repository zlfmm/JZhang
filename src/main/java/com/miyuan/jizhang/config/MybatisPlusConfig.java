package com.miyuan.jizhang.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;

/**
 * 开启mybatis-plus分页功能
 * @author zhanglongfei
 *
 */
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {
	@Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
