package com.miyuan.jizhang.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSON;
import com.miyuan.jizhang.enums.ExceptionCodeEnum;
import com.miyuan.jizhang.listener.SessionListener;
import com.miyuan.jizhang.response.BaseResult;
import com.miyuan.jizhang.response.LoginUserInfoResponseVo;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	private String[] filterUriList = {"/login", "/logout"};
	
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws IOException {
		boolean flag = false;
        response.setContentType("text/html;charset=utf-8");
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Object bean = handlerMethod.getBean();
            if (bean instanceof BasicErrorController) {
                java.io.PrintWriter out = response.getWriter();
                BaseResult rsp = new BaseResult(ExceptionCodeEnum.SUTR_ERROR);
                out.write(JSON.toJSONString(rsp));
                return false;
            }
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("curUser") != null) {
        	LoginUserInfoResponseVo tu = (LoginUserInfoResponseVo) session.getAttribute("curUser");
            HttpSession hs = (HttpSession) SessionListener.sessionMap.get(tu.getId());
            if (null != hs) {
                flag = true;
            }
        } else {
            String uri = request.getRequestURI();
            flag = validateUri(uri);
        }
        if (!flag) {
        	// 跳转到登录页
        	response.sendRedirect(request.getContextPath() + "/tologin");
        }
        return flag;
    }

	public boolean validateUri(String uri) {
        boolean flag = false;
        for (String item : filterUriList) {
            if (uri.startsWith(item)) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
