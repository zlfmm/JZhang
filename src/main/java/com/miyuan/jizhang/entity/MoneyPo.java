package com.miyuan.jizhang.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@TableName("my_money")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MoneyPo {
	
	@Id
	@TableId(type = IdType.AUTO, value = "jz_id")
	private Integer id;
	
	@TableField(value = "jz_user_group_id")
	private Integer userGroupId;
	
	@TableField(value = "jz_money")
	private BigDecimal money;
	
	@TableField(value = "jz_pay_time")
	private String payTime;
	
	@TableField(value = "jz_desc")
	private String desc;
	
	@TableField(value = "jz_is_settle")
	private Integer isSettle = 0;
	
	@TableField(value = "jz_settle_time")
	private String settleTime;
	
	@TableField(value = "jz_create_time")
	private String createTime;

	@TableField(value = "jz_del")
	private Integer isDel = 0;

}
