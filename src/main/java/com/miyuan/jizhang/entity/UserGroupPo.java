package com.miyuan.jizhang.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 用户和群——映射表
 * @author zhanglongfei
 *
 */
@Entity
@TableName("my_user_group")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupPo {
	
	@Id
	@TableId(type = IdType.AUTO, value = "jz_id")
	private Integer id;
	
	@TableField(value = "jz_group_id")
	private Integer groupId;
	
	@TableField(value = "jz_group_name")
	private String groupName;
	
	@TableField(value = "jz_user_id")
	private Integer userId;
	
	@TableField(value = "jz_real_name")
	private String realName;
	
	@TableField(value = "jz_create_time")
	private String createTime;
	
	@TableField(value = "jz_del")
	private Integer isDel = 0;

}
