package com.miyuan.jizhang.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 群组：群内用户进行记账相关操作
 * @author zhanglongfei
 *
 */
@Entity
@TableName("my_group")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GroupPo {

	@Id
	@TableId(type = IdType.AUTO, value = "jz_id")
	private Integer id;

	@TableField(value = "jz_name")
	private String name;

	@TableField(value = "jz_desc")
	private String desc;
	
	@TableField(value = "jz_create_user")
	private Integer createUser;

	@TableField(value = "jz_create_time")
	private String createTime;

	@TableField(value = "jz_del")
	private Integer isDel = 0;

}
