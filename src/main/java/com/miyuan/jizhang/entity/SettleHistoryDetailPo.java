package com.miyuan.jizhang.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@TableName("my_settle_history_detail")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SettleHistoryDetailPo {
	
	@Id
	@TableId(type = IdType.AUTO, value = "jz_id")
	private Integer id;
	
	@TableField(value = "jz_settle_id")
	private Integer settleId;
	
	@TableField(value = "jz_send_user_id")
	private Integer sendUserId;
	
	@TableField(value = "jz_receive_user_id")
	private Integer receiveUserId;
	
	@TableField(value = "jz_money")
	private BigDecimal money;
	
	@TableField(value = "jz_create_time")
	private String createTime;
	
	@TableField(value = "jz_del")
	private Integer isDel = 0;

}
