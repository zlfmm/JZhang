package com.miyuan.jizhang.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@TableName("my_settle")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SettlePo {
	
	@Id
	@TableId(type = IdType.AUTO, value = "jz_id")
	private Integer id;
	
	@TableField(value = "jz_group_id")
	private Integer groupId;
	
	@TableField(value = "jz_settle_total_money")
	private BigDecimal settleTotalMoney;
	
	@TableField(value = "jz_create_time")
	private String createTime;
	
	@TableField(value = "jz_del")
	private Integer isDel = 0;

}
