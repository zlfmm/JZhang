package com.miyuan.jizhang.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "根据群ID修改群信息请求参数类", description = "根据群ID修改群信息请求参数类")
@Data
@ToString
public class UpdateGroupRequestVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5058509099826752766L;
	
	@NotNull(message = "请求参数群ID不能为空")
	@ApiModelProperty(value = "群ID")
	private Integer id;
	
	@ApiModelProperty(value = "群名称")
	private String name;
	
	@ApiModelProperty(value = "群描述")
	private String desc;

}
