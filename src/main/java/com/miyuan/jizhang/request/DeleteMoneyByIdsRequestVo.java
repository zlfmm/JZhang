package com.miyuan.jizhang.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "根据记录id删除消费记录请求参数类", description = "根据记录id删除消费记录请求参数类")
@Data
@ToString
public class DeleteMoneyByIdsRequestVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5895177787974903188L;
	
	@NotNull(message = "请求参数消费记录ID集合不能为空")
	@ApiModelProperty(value = "消费记录ID集合")
	private List<Integer> ids;


}
