package com.miyuan.jizhang.request;

import java.io.Serializable;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询消费记录列表请求参数类", description = "查询消费记录列表请求参数类")
@Data
@ToString
public class ListMoneyRequestVo extends BaseRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5601330468502714507L;
	
	@ApiModelProperty(value = "群组ID，暂时没用到")
	private Integer groupId;
	
	@ApiModelProperty(value = "用户ID，暂时没用到")
	private Integer userId;
	
	@ApiModelProperty(value = "是否已结算（1：已结算；0：未结算）")
	private Integer isSettle;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间", notes = "开始时间")
	private String payStartTime;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间", notes = "结束时间")
	private String payEndTime;
	
	@ApiModelProperty(value = "是否有效:1无效；0有效")
	private Integer isDel = 0;

}
