package com.miyuan.jizhang.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "新增用户信息请求参数类", description = "新增用户信息请求参数类")
@Data
@ToString
public class InsertUserInfoRequestVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6043242927965975485L;
	
	@NotNull(message = "用户名不能为空")
	@Length(min=8, max=16, message="用户名长度为8-16位")
	@ApiModelProperty(value = "用户名")
	private String userName;
	
	@NotNull(message = "密码不能为空，请输入用户名")
	@ApiModelProperty(value = "密码")
	private String passWord;
	
	@NotNull(message = "真实姓名不能为空，请输入真实姓名")
	@ApiModelProperty(value = "真实姓名")
	private String realName;
	
	@NotNull(message = "邮箱不能为空，请输入邮箱")
	@Pattern(regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", message = "邮箱格式有误")
	@ApiModelProperty(value = "邮箱")
	private String userAddress;

}
