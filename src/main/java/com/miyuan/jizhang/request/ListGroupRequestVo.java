package com.miyuan.jizhang.request;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询用户信息列表请求参数类", description = "查询用户信息列表请求参数类")
@Data
@ToString
public class ListGroupRequestVo extends BaseRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6948704605664314179L;
	
	@ApiModelProperty(value = "群ID")
	private Integer id;
	
	@ApiModelProperty(value = "群名称")
	private String name;
	
	@ApiModelProperty(value = "群描述")
	private String desc;

}
