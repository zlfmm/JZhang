package com.miyuan.jizhang.request;

import java.io.Serializable;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "分页请求参数基类", description = "分页请求参数基类")
@Data
@ToString
public class BaseRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2843606453518703252L;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间", notes = "开始时间")
	private String startTime;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间", notes = "结束时间")
	private String endTime;

	@ApiModelProperty(value = "页码数", notes = "默认第1页")
	private Integer pageNumber = 1;
	
	@ApiModelProperty(value = "每页条数", notes = "默认10条")
	private Integer pageSize = 10;
	
}
