package com.miyuan.jizhang.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "申请好友请求参数类", description = "申请好友请求参数类")
@Data
@ToString
public class InsertFriendRequestVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2087850538476653114L;
	
	@NotNull(message = "接收方不能为空")
	@ApiModelProperty(value = "接收方Id集合，支持批量申请")
	private List<Integer> receiveUserIds;
	
	@ApiModelProperty(value = "申请加好友备注")
	private String remark;

}
