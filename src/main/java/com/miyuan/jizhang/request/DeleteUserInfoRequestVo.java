package com.miyuan.jizhang.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "根据用户ID删除用户信息请求参数类", description = "根据用户ID删除用户信息请求参数类")
@Data
@ToString
public class DeleteUserInfoRequestVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3079100002360086707L;
	
	@NotNull(message = "请求参数用户ID集合不能为空")
	@ApiModelProperty(value = "用户ID")
	private List<Integer> ids;

}
