package com.miyuan.jizhang.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "根据用户ID修改用户信息请求参数类", description = "根据用户ID修改用户信息请求参数类")
@Data
@ToString
public class UpdateUserInfoRequestVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6012593204538330736L;

	@NotNull(message = "请求参数用户ID不能为空")
	@ApiModelProperty(value = "用户ID")
	private Integer id;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "密码")
	private String passWord;
	
	@ApiModelProperty(value = "邮箱")
	private String userAddress;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

}
