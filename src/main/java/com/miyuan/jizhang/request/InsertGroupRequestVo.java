package com.miyuan.jizhang.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "新增群信息请求参数类", description = "新增群信息请求参数类")
@Data
@ToString
public class InsertGroupRequestVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8936211137208642999L;
	
	@NotNull(message = "群名称不能为空，请输入群名称")
	@Length(min=6, max=12, message="群名称长度必须在6-12之间")
	@ApiModelProperty(value = "群名称")
	private String name;
	
	@ApiModelProperty(value = "群描述")
	private String desc;	

}
