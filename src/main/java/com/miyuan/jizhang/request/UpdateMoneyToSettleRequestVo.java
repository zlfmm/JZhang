package com.miyuan.jizhang.request;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "根据ID批量结算消费记录请求参数类", description = "根据ID批量结算消费记录请求参数类")
@Data
@ToString
public class UpdateMoneyToSettleRequestVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2986454672326074216L;
	
	@ApiModelProperty(value = "消费记录ID集合")
	private List<Integer> ids;
	
	@ApiModelProperty(value = "参与结算的用户ID集合")
	private List<Integer> userIds;

}
