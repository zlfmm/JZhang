package com.miyuan.jizhang.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "根据群ID删除群信息请求参数类", description = "根据群ID删除群信息请求参数类")
@Data
@ToString
public class DeleteGroupRequestVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8495875148335193703L;
	
	@NotNull(message = "请求参数群ID集合不能为空")
	@ApiModelProperty(value = "群ID")
	private List<Integer> ids;

}
