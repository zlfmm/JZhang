package com.miyuan.jizhang.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "分页查询消费列表详情请求参数类", description = "分页查询消费列表详情请求参数类")
@Data
@ToString
public class ListSettleHistoryRequestVo extends BaseRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3678398846118804083L;
	
	@ApiModelProperty(value = "结算主表ID")
	@NotNull(message = "请求参数结算主表ID不能为空")
	private Integer settleId;

}
