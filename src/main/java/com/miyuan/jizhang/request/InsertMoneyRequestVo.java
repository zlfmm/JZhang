package com.miyuan.jizhang.request;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "新增金额记录请求参数类", description = "新增金额记录请求参数类")
@Data
@ToString
public class InsertMoneyRequestVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8821652513766221431L;
	
	@NotNull(message = "群组ID不能为空")
	@ApiModelProperty(value = "所属群组ID")
	private Integer userGroupId;
	
	@NotNull(message = "金额不能为空")
	@ApiModelProperty(value = "消费金额")
	private BigDecimal money;
	
	@NotNull(message = "消费时间不能为空")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String payTime;
	
	@ApiModelProperty(value = "消费描述")
	private String desc;

}
