package com.miyuan.jizhang.request;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询用户信息列表请求参数类", description = "查询用户信息列表请求参数类")
@Data
@ToString
public class ListUserInfoRequestVo extends BaseRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7144462655254210659L;

	@ApiModelProperty(value = "用户ID")
	private Integer id;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "密码")
	private String passWord;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

}
