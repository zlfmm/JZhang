package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询群信息列表返回参数类", description = "查询群信息列表返回参数类")
@Data
@ToString
public class ListGroupResponseVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1201409564564331668L;
	
	@ApiModelProperty(value = "群ID")
	private Integer id;

	@ApiModelProperty(value = "群名称")
	private String aname;

	@ApiModelProperty(value = "群描述")
	private String adesc;

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty(value = "是否有效:1无效；0有效")
	private Integer isDel = 0;

}
