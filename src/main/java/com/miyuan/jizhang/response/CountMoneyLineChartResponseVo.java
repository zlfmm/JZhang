package com.miyuan.jizhang.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ApiModel(value = "统计本期消费金额折线图数据返回参数类", description = "统计本期消费金额折线图数据返回参数类")
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CountMoneyLineChartResponseVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -901767843829732237L;
	
	@ApiModelProperty(value = "横坐标数据")
	private String label;
	
	@ApiModelProperty(value = "纵坐标数据")
	private Double dataset;

}
