package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "登录返回参数类", description = "登录返回参数类")
@Data
@ToString
public class LoginUserInfoResponseVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5173630526724755970L;
	
	@ApiModelProperty(value = "数据库ID")
	private Integer id;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "真实姓名")
	private String realName;
	
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty(value = "是否删除（1：删除；0：未删除）")
	private Integer isDel = 0;

}
