package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询消费记录列表返回参数类", description = "查询消费记录列表返回参数类")
@Data
@ToString
public class ListMoneyResponseVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4880430073900843777L;
	
	@ApiModelProperty(value = "数据库ID")
	private Integer id;
	
	@ApiModelProperty(value = "用户群组关联表ID")
	private Integer userGroupId;
	
	@ApiModelProperty(value = "真实姓名")
	private String realName;
	
	@ApiModelProperty(value = "消费金额")
	private BigDecimal money;
	
	@ApiModelProperty(value = "消费时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String payTime;
	
	@ApiModelProperty(value = "消费描述")
	private String desc;
	
	@ApiModelProperty(value = "是否已结算（1：已结算；0：未结算）")
	private Integer isSettle;
	
	@ApiModelProperty(value = "结算时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String settleTime;
	
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String createTime;
	
	@ApiModelProperty(value = "是否有效:1无效；0有效")
	private Integer isDel = 0;

}
