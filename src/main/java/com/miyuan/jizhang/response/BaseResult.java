package com.miyuan.jizhang.response;

import com.miyuan.jizhang.enums.ExceptionCodeEnum;

import lombok.Data;
import lombok.ToString;

/**
 * 返回基础类
 * 
 * @author zhanglongfei
 *
 * @param <T>
 */
@Data
@ToString
public class BaseResult<T> {

	private T data;
	private Integer errCode = 200;
	private String errMsg;

	public BaseResult() {
	}

	public BaseResult(T data) {
		super();
		this.data = data;
	}

	public BaseResult(T data, Integer errCode, String errMsg) {
		super();
		this.data = data;
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public BaseResult(Integer errCode, String errMsg) {
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public BaseResult(ExceptionCodeEnum enums) {
		this.errCode = enums.getCode();
		this.errMsg = enums.getMsg();
	}

	public BaseResult(T data, ExceptionCodeEnum enums) {
		this.data = data;
		this.errCode = enums.getCode();
		this.errMsg = enums.getMsg();
	}

}
