package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ApiModel(value = "查询结算记录列表返回参数类", description = "查询结算记录列表返回参数类")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ListSettleHistoryResponseVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8475376601833922763L;
	
	@ApiModelProperty(value = "数据库ID")
	private Integer id;
	
	@ApiModelProperty(value = "结算主表ID")
	private Integer settleId;
	
	@ApiModelProperty(value = "用户ID")
	private Integer userId;
	
	@ApiModelProperty(value = "用户名称")
	private String realName;
	
	@ApiModelProperty(value = "群组ID")
	private Integer groupId;
	
	@ApiModelProperty(value = "群组名称")
	private String groupName;
	
	@ApiModelProperty(value = "结算金额")
	private BigDecimal settleMoney;	

}
