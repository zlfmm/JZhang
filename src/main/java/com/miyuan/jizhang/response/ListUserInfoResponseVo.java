package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询用户信息列表返回参数类", description = "查询用户信息列表返回参数类")
@Data
@ToString
public class ListUserInfoResponseVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3950704201609956898L;
	
	@ApiModelProperty(value = "用户ID")
	private Integer id;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "密码")
	private String passWord;
	
	@ApiModelProperty(value = "邮箱")
	private String userAddress;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty(value = "是否有效:1无效；0有效")
	private Integer isDel = 0;
	

}
