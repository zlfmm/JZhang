package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ApiModel(value = "查询结算记录主表列表返回参数类", description = "查询结算记主表录列表返回参数类")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ListSettleResponseVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8475376601833922763L;
	
	@ApiModelProperty(value = "数据库ID")
	private Integer id;
	
	@ApiModelProperty(value = "结算金额")
	private BigDecimal settleTotalMoney;
	
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String createTime;

}
