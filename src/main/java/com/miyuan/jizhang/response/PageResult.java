package com.miyuan.jizhang.response;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PageResult<T> {

	private int total;// 总记录数
	
	private int pageSize = 10;// 每页记录数

	private int pageNumber = 1;// 当前页码

	private int totalPage;// 总页数

	private List<T> data;
	
	public int getTotalPage() {
		return totalPage = (this.getTotal() % this.getPageSize() == 0
				? (this.getTotal() / this.getPageSize())
				: (this.getTotal() / this.getPageSize() + 1));
	}
	
	public PageResult(List<T> data, int pageNumber, int pageSize, int total) {
		super();
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
		this.total = total;
		this.data = data;
	}

	public PageResult() {
		super();
	}


}
