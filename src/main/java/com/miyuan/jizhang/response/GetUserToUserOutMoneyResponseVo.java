package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ApiModel(value = "查询结算详情之明细信息返回参数类", description = "查询结算详情之明细信息返回参数类")
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GetUserToUserOutMoneyResponseVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -760271709391988620L;
	
	@ApiModelProperty(value = "支出人Id")
	private Integer sendUserId;
	
	@ApiModelProperty(value = "支出人姓名")
	private String sendUserName;
	
	@ApiModelProperty(value = "收入人Id")
	private Integer receiveUserId;
	
	@ApiModelProperty(value = "收入人姓名")
	private String receiveUserName;
	
	@ApiModelProperty(value = "金额")
	private BigDecimal money;

}
