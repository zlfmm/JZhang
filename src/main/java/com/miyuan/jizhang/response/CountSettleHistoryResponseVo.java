package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询结算总金额返回参数类", description = "查询结算总金额返回参数类")
@Data
@ToString
public class CountSettleHistoryResponseVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 401069390925556276L;
	
	@ApiModelProperty(value = "结算金额")
	private BigDecimal totalSettleMoney;

}
