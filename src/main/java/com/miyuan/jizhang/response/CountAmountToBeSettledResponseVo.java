package com.miyuan.jizhang.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ApiModel(value = "统计本期待结算总金额返回参数类", description = "统计本期待结算总金额返回参数类")
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CountAmountToBeSettledResponseVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5279627403707252270L;
	
	@ApiModelProperty(value = "待结算总金额")
	private BigDecimal total;
	
	@ApiModelProperty(value = "平均每人消费金额")
	private BigDecimal average;
	
	@ApiModelProperty(value = "每人实际消费详情")
	private List<String> values;

}
