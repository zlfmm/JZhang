package com.miyuan.jizhang.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "查询好友申请列表返回参数类", description = "查询好友申请列表返回参数类")
@Data
@ToString
public class ListApplyFriendResponseVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1226685538751017656L;
	
	@ApiModelProperty(value = "数据库Id")
	private Integer id;
	
	@ApiModelProperty(value = "申请人姓名")
	private String applyUserName;
	
	@ApiModelProperty(value = "申请人邮箱")
	private String applyUserAddress;
	
	@ApiModelProperty(value = "备注")
	private String remark;

}
