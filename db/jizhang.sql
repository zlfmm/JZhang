-- -----------------------------------------------------------
-- jizhang
-- 数据库采用utf-8的字符集
-- -----------------------------------------------------------
##填入最新版本号
set @newversion = '1.0.0';

##判断数据库是否存在，不存在则创建数据库
create database if not exists jizhang DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

##进入数据库
use jizhang;

set names utf8mb4;

-- -----------------------------------------------------------
-- 版本由0.0.0升级到1.0.0的过程  没创建的表进行创建
-- -----------------------------------------------------------
drop procedure if exists ap_proc_ver0tover1;
delimiter //
create procedure ap_proc_ver0tover1()
begin
	declare indxcnt int;
	declare tabexit int;
	declare trigexit int;
	declare colexit int;
	SET FOREIGN_KEY_CHECKS=1;

	-- ----------------------------
	-- Table structure for db_version
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='db_version';
	if tabexit <= 0 then
		CREATE TABLE `db_version` (
		  `version` varchar(16) NOT NULL,
		  `upgrade_time` datetime NOT NULL 
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
	end if;

	-- ----------------------------
	-- Records of db_version
	-- ----------------------------
	DELETE FROM `db_version`;
	INSERT INTO `db_version` VALUES ('0.0.0', now());
	
	
	
	-- ----------------------------
	--  Table structure for `my_group`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_group';
	if tabexit <= 0 then
		CREATE TABLE `my_group` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
		  `jz_name` varchar(30) NOT NULL COMMENT '群名称',
		  `jz_desc` varchar(255) DEFAULT NULL COMMENT '群描述',
		  `jz_create_user` int(11) NOT NULL COMMENT '创建人ID',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群：群内用户进行记账相关操作';
	end if;
	
	-- ----------------------------
	--  Table structure for `my_money`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_money';
	if tabexit <= 0 then
		CREATE TABLE `my_money` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
		  `jz_user_group_id` int(11) NOT NULL COMMENT '用户组关联表ID',
		  `jz_money` decimal(10,0) NOT NULL COMMENT '金额',
		  `jz_pay_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '消费时间',
		  `jz_desc` varchar(255) DEFAULT NULL COMMENT '描述',
		  `jz_is_settle` int(1) DEFAULT '0' COMMENT '是否已结算（1：已结算；0：未结算）',
		  `jz_settle_time` timestamp NULL DEFAULT NULL COMMENT '结算时间',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消费金额记录表';
	end if;
	
	-- ----------------------------
	--  Table structure for `my_settle`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_settle';
	if tabexit <= 0 then
		CREATE TABLE `my_settle` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
		  `jz_group_id` int(11) NOT NULL COMMENT '群组关联表ID',
		  `jz_settle_total_money` varchar(255) NOT NULL COMMENT '结算总金额',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算主表';
	end if;
	
	-- ----------------------------
	--  Table structure for `my_settle_history`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_settle_history';
	if tabexit <= 0 then
		CREATE TABLE `my_settle_history` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
		  `jz_settle_id` int(11) NOT NULL COMMENT '结算主表ID',
		  `jz_user_group_id` int(11) NOT NULL COMMENT '用户组关联表ID',
		  `jz_settle_money` decimal(10,2) NOT NULL COMMENT '结算金额',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算消费历史表';
	end if;
	
	-- ----------------------------
	--  Table structure for `my_settle_history_detail`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_settle_history_detail';
	if tabexit <= 0 then
		CREATE TABLE `my_settle_history_detail` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
		  `jz_settle_id` int(11) NOT NULL COMMENT '结算主表ID',
		  `jz_send_user_id` int(11) NOT NULL COMMENT '支出方用户ID',
		  `jz_receive_user_id` int(11) NOT NULL COMMENT '收入方用户ID',
		  `jz_money` decimal(10,2) NOT NULL COMMENT '金额',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算详情之明细表';
	end if;
	
	-- ----------------------------
	--  Table structure for `my_user_group`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_user_group';
	if tabexit <= 0 then
		CREATE TABLE `my_user_group` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
		  `jz_group_id` int(11) NOT NULL COMMENT '群ID',
		  `jz_group_name` varchar(30) DEFAULT NULL COMMENT '群组名',
		  `jz_user_id` int(11) NOT NULL COMMENT '用户ID',
		  `jz_real_name` varchar(15) DEFAULT NULL COMMENT '真实姓名',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户和群——映射表';
	end if;
	
	-- ----------------------------
	--  Table structure for `my_user_info`
	-- ----------------------------
	select count(table_name) into tabexit from information_schema.tables where table_schema='jizhang' and table_name='my_user_info';
	if tabexit <= 0 then
		CREATE TABLE `my_user_info` (
		  `jz_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
		  `jz_user_name` varchar(20) NOT NULL COMMENT '用户名',
		  `jz_pass_word` varchar(60) NOT NULL COMMENT '密码',
		  `jz_user_address` varchar(30) DEFAULT NULL COMMENT '用户注册的邮箱',
		  `jz_real_name` varchar(15) NOT NULL COMMENT '真实姓名',
		  `jz_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  `jz_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0：未删除）',
		  PRIMARY KEY (`jz_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';
	end if;

end;
// 
delimiter ;


-- -----------------------------------------------------------
-- 升级的总控制存储过程
-- -----------------------------------------------------------
##通过数据库存数过程来进行数据库升级
drop procedure if exists ap_proc_upgrade;
delimiter //
create procedure ap_proc_upgrade(in newver varchar(10))
begin 
-- 获取数据库中原有版本信息
declare oldver varchar(10);
declare oldtmp varchar(10);
declare vcnt int;
declare vmsgcnt int;
set oldtmp = '0.0.0';

select count(*) into vcnt from information_schema.tables where table_schema='jizhang' and table_name='db_version';
if vcnt > 0 then
    select count(*) into vmsgcnt from db_version;
    if vmsgcnt > 0 then
        select version into oldtmp from db_version limit 0,1;
    else 
        insert into db_version(version, upgrade_time) values('0.0.0', now());
    end if;
end if;

set oldver = oldtmp;

-- 比较新旧版本进行升级,如果当前版本低于升级版本
while strcmp(oldtmp, newver) = -1 do
begin
    if strcmp(oldtmp, '1.0.0') = -1 then
        call ap_proc_ver0tover1();
        update db_version set version = '1.0.0',upgrade_time = now() where version= oldtmp;
	set oldtmp = '1.0.0';
    end if;
        
end;
end while;


select oldver;
select newver;

end;
//
delimiter ;

##调用数据存数过程进行升级
call ap_proc_upgrade(@newversion);

##释放掉数据库函数
drop procedure ap_proc_upgrade;
drop procedure ap_proc_ver0tover1;

##提交整个操作
commit;